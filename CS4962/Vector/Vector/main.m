//
//  main.m
//  Vector
//
//  Created by Adam Bradford on 1/16/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VECAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VECAppDelegate class]));
    }
}
