//
//  VECAppDelegate.m
//  Vector
//
//  Created by Adam Bradford on 1/16/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "VECAppDelegate.h"
#import "VECVector.h"
#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>

@implementation VECAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    //Create window
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[_window setBackgroundColor:[UIColor grayColor]];
    [_window makeKeyAndVisible];
    
    
    //Create viewController
    UIViewController* viewController = [[UIViewController alloc] init];
    [_window setRootViewController:viewController];
    viewController.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    
    //set up notifications of rotation
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(orientationChanged)
     name:UIDeviceOrientationDidChangeNotification
     object:[UIDevice currentDevice]];

	
	// Create decorative splash image
	_splashImageView = [[UIImageView alloc]init];
	_splashImageView.image = [UIImage imageNamed:@"blackboard.jpg"];
	_splashImageView.contentMode = UIViewContentModeTopLeft;
    _splashImageView.translatesAutoresizingMaskIntoConstraints = NO;
    _splashImageView.clipsToBounds = YES;
    [self addShadowtoView:_splashImageView];
    [viewController.view addSubview:_splashImageView];
    
    
    //Create _vectorList and set up defaults and Paramaters
    _vectorList = [[UITextView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 300.0f)];
    _vectorList.text = @"Please enter a Vector\n" ;
    _vectorList.textAlignment = NSTextAlignmentCenter;
    _vectorList.translatesAutoresizingMaskIntoConstraints = NO;
    _vectorList.editable = false;
    _vectorList.textColor = [UIColor whiteColor];
    _vectorList.backgroundColor = [UIColor clearColor];
    _vectorList.font = [UIFont systemFontOfSize:17];
    [self addShadowtoView:_vectorList];
    [viewController.view addSubview:_vectorList];
    
    
    //Create _sliderX and set up defaults and Paramaters
    _sliderX = [[UISlider alloc] init];
    _sliderX.minimumValue = -1.0f;
    _sliderX.maximumValue = 1.0f;
    _sliderX.value = 0.0f;
    _sliderX.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoView:_sliderX];
    [viewController.view addSubview:_sliderX];
    
    
    //Create _labelX and set up defaults and Paramaters
    UILabel* labelX = [[UILabel alloc]init];
    labelX.text = @"X";
    labelX.textAlignment = NSTextAlignmentLeft;
    labelX.backgroundColor = [UIColor clearColor];
    labelX.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoText:labelX];
    [viewController.view addSubview:labelX];
    
    
    //Create _countLabelX and set up defaults and Paramaters
    _countLabelX = [[UILabel alloc]init];
    _countLabelX.textAlignment = NSTextAlignmentRight;
    _countLabelX.text = [NSString stringWithFormat: @"%.2f",_sliderX.value ];
	_countLabelX.backgroundColor = [UIColor clearColor];
    _countLabelX.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoText:_countLabelX];
	[viewController.view  addSubview:_countLabelX];
    
    
    //Create _sliderY and set up defaults and Paramaters
    _sliderY = [[UISlider alloc] init];
    _sliderY.minimumValue = -1.0f;
    _sliderY.maximumValue = 1.0f;
    _sliderY.value = 0.0f;
    _sliderY.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoView:_sliderY];
    [viewController.view addSubview:_sliderY];
    
    
    //Create LabelY and set up defaults and Paramaters
    UILabel* labelY = [[UILabel alloc]init];
    labelY.text = @"Y";
    labelY.translatesAutoresizingMaskIntoConstraints = NO;
    labelY.backgroundColor = [UIColor clearColor];
    [self addShadowtoText:labelY];
    [viewController.view addSubview:labelY];
    
    
    //Create _countLabelY and set up defaults and Paramaters
    _countLabelY = [[UILabel alloc]init];
    _countLabelY.textAlignment = NSTextAlignmentRight;
    _countLabelY.text = [NSString stringWithFormat: @"%.2f",_sliderY.value ];
	_countLabelY.backgroundColor = [UIColor clearColor];
    _countLabelY.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoText:_countLabelY];
	[viewController.view addSubview:_countLabelY];
    
    
    //Create _sliderZ and set up defaults and Paramaters
    _sliderZ = [[UISlider alloc] init];
    _sliderZ.minimumValue = -1.0f;
    _sliderZ.maximumValue = 1.0f;
    _sliderZ.value = 0.0f;
    _sliderZ.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoView:_sliderZ];
    [viewController.view addSubview:_sliderZ];
    
    
    //Create _labelZ and set up defaults and Paramaters
    UILabel* labelZ = [[UILabel alloc]init];
    labelZ.text = @"Z";
    labelZ.backgroundColor = [UIColor clearColor];
    labelZ.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoText:labelZ];
    [viewController.view addSubview:labelZ];
    
    
    //Create _countLabelZ and set up defaults and Paramaters
    _countLabelZ = [[UILabel alloc]init];
    _countLabelZ.textAlignment = NSTextAlignmentRight;
    _countLabelZ.text = [NSString stringWithFormat: @"%.2f",_sliderZ.value ];
	_countLabelZ.backgroundColor = [UIColor clearColor];
    _countLabelZ.translatesAutoresizingMaskIntoConstraints = NO;
    [self addShadowtoText:_countLabelZ];
	[viewController.view addSubview:_countLabelZ];
    
   
    //Create _longestLabel and set up defaults and Paramaters
    _longestLabel = [[UILabel alloc]init];
    _longestLabel.text = @"Longest Vector : N/A";
    _longestLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _longestLabel.backgroundColor = [UIColor clearColor];
    _longestLabel.textAlignment = NSTextAlignmentCenter;
    _longestLabel.adjustsFontSizeToFitWidth = YES;
    _longestLabel.font = [UIFont systemFontOfSize:17];
    [self addShadowtoText:_longestLabel];
    [viewController.view addSubview:_longestLabel];
    
    
    //Create _productLabel and set up defaults and Paramaters
    _productLabel = [[UILabel alloc]init];
    _productLabel.text = @"Vector :2 * Vector:1: N/A";
    _productLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _productLabel.backgroundColor = [UIColor clearColor];
    _productLabel.textAlignment = NSTextAlignmentCenter;
    _productLabel.font = [UIFont systemFontOfSize:17];
    _productLabel.adjustsFontSizeToFitWidth = YES;
    [self addShadowtoText:_productLabel];
    [viewController.view addSubview:_productLabel];
    
    
    //Create _sumLabel and set up defaults and Paramaters
    _sumLabel = [[UILabel alloc]init];
    _sumLabel.text = @"Sum of All Vectors = N/A";
    _sumLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _sumLabel.backgroundColor = [UIColor clearColor];
    _sumLabel.textAlignment = NSTextAlignmentCenter;
    _sumLabel.font = [UIFont systemFontOfSize:17];
    _sumLabel.adjustsFontSizeToFitWidth = YES;
    [self addShadowtoText:_sumLabel];
    [viewController.view addSubview:_sumLabel];
    
    
    //Create _lastVectorLabel and set up defaults and Paramaters
    _lastVectorLabel = [[UILabel alloc]init];
    _lastVectorLabel.text = @"Last Vector: N/A";
    _lastVectorLabel.translatesAutoresizingMaskIntoConstraints = NO;
    _lastVectorLabel.backgroundColor = [UIColor clearColor];
    _lastVectorLabel.textAlignment = NSTextAlignmentCenter;
    _lastVectorLabel.font = [UIFont systemFontOfSize:17];
    _lastVectorLabel.adjustsFontSizeToFitWidth = YES;
    [self addShadowtoText:_lastVectorLabel];
    [viewController.view addSubview:_lastVectorLabel];
    
	
    //set up sliders and add actions
    [_sliderX addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_sliderY addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_sliderZ addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    //Create _boldLabel and setup defaults and paramaters
    //Create _lastVectorLabel and set up defaults and Paramaters
    UILabel* boldLabel = [[UILabel alloc]init];
    boldLabel.text = @"Use Sliders and Press to Enter a Vector";
    boldLabel.translatesAutoresizingMaskIntoConstraints = NO;
    boldLabel.backgroundColor = [UIColor clearColor];
    boldLabel.textAlignment = NSTextAlignmentCenter;
    boldLabel.adjustsFontSizeToFitWidth = YES;
    boldLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:11];
    [self addShadowtoText:boldLabel];
    [viewController.view addSubview:boldLabel];
    
    //Create _vectorButton and set up defaults and Paramaters
    _vectorButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    _vectorButton.translatesAutoresizingMaskIntoConstraints = NO;
    [_vectorButton setTitle:@"Calculate Vector" forState:UIControlStateNormal];
    [_vectorButton addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addShadowtoView:_vectorButton];
    [viewController.view addSubview:_vectorButton];
    
    //EmptyView used for autolayout
    UIView *emptySpace = [[UIView alloc]init];
    emptySpace.translatesAutoresizingMaskIntoConstraints = NO;
    [viewController.view addSubview:emptySpace];
    
    //Create dictionary of bindings for use in autoLayout
    NSMutableDictionary *bindings = [[NSMutableDictionary alloc]init];
    [bindings addEntriesFromDictionary:NSDictionaryOfVariableBindings(_splashImageView,_vectorList,_sliderX,_sliderY,_sliderZ,labelX,labelY,labelZ,_countLabelX,_countLabelY,_countLabelZ,boldLabel,_vectorButton,_sumLabel,_productLabel,_lastVectorLabel,_longestLabel,emptySpace)];
    
    //Horizontal Constratints for autolayout
    [self addVisualConstraint:@"|[_splashImageView]|" withBindings:bindings];
    [self addVisualConstraint:@"|[_vectorList]|" withBindings:bindings];
    [self addVisualConstraint:@"|-[_longestLabel]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[_productLabel]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[_sumLabel]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[boldLabel]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[_lastVectorLabel]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[labelX(==20)]-[_sliderX]-[_countLabelX(==40)]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[labelY(==20)]-[_sliderY]-[_countLabelY(==40)]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[labelZ(==20)]-[_sliderZ]-[_countLabelZ(==40)]-|" withBindings:bindings];
    [self addVisualConstraint:@"|-[_vectorButton]-|" withBindings:bindings];
    
    //Vertical Constraints for autolayout
    [self addVisualConstraint:@"V:|[_splashImageView(==10)][_vectorList(>=50,<=250)]-[_longestLabel(==20)][_productLabel(==20)][_sumLabel(==20)][_lastVectorLabel(==20)]-[_sliderX(==20)]-[_sliderY(==20)]-[_sliderZ(==20)]-[boldLabel(==10)]-[_vectorButton(<=30)]-[emptySpace(>=1)]|" withBindings:bindings];
    [self addVisualConstraint:@"V:[_lastVectorLabel]-[labelX(==20)]" withBindings:bindings];
    [self addVisualConstraint:@"V:[labelX]-[labelY(==20)]" withBindings:bindings];
    [self addVisualConstraint:@"V:[labelY]-[labelZ(==20)]" withBindings:bindings];
    [self addVisualConstraint:@"V:[_lastVectorLabel]-[_countLabelX(==20)]" withBindings:bindings];
    [self addVisualConstraint:@"V:[_sliderX]-[_countLabelY(==20)]" withBindings:bindings];
    [self addVisualConstraint:@"V:[_sliderY]-[_countLabelZ(==20)]" withBindings:bindings];
    
    
   
    return 0;
  
}

//Updates _count lables when slider value changes.
-(IBAction)sliderValueChanged:(UISlider *)sender
{
    if(sender == _sliderX)
    {
        _countLabelX.text = [NSString stringWithFormat:@"%.2f",_sliderX.value];
    }
    if(sender == _sliderY)
    {
        _countLabelY.text = [NSString stringWithFormat:@"%.2f",_sliderY.value];
    }
    if(sender == _sliderZ)
    {
        _countLabelZ.text = [NSString stringWithFormat:@"%.2f",_sliderZ.value];
    }
}

//Changes label's text color to white and adds a shadow
-(void)addShadowtoText:(UILabel *)label
{
    label.textColor = [UIColor whiteColor];
    [self addShadowtoView:label];
}

//Adds a shadow to a view
-(void)addShadowtoView:(UIView *)view
{
    view.layer.shadowColor = [[UIColor blackColor]CGColor];
    view.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    view.layer.shadowOpacity = 1.0f;
    view.layer.shadowRadius = 1.0f;
}

//Action when button is pressed
-(void)buttonPressed
{
    //lazy instantiation for _magnitudeList and _vectorArray
    if(!_magnitudeList)
    {
        
        _magnitudeList = [[NSMutableArray alloc]init];
    }
    if(!_vectorArray)
    {
        _vectorArray = [[NSMutableArray alloc]init];
    }
    
    //set _vectorArray default value
    if(_vectorArray.count == 0)
    {
        _vectorList.text = @"";
    }
    
    //Create temp vector from slider values
    VECVector *temp = [[VECVector alloc]initWithCoordinates_X:_sliderX.value Y:_sliderY.value Z:_sliderZ.value];
    
    //Add temp to the vector array
    [_vectorArray addObject:temp];
    
    //Add description of temp to the UIScrollView
    _vectorList.text = [_vectorList.text stringByAppendingFormat:@"Vector %d: %@\n",_magnitudeList.count +1,  temp ];
    
    //This handles autoscrolling the scroll view.
    if(_vectorList.contentSize.height > _vectorList.bounds.size.height)
    {
        CGPoint offset = CGPointMake(0, _vectorList.contentSize.height - _vectorList.bounds.size.height-_vectorList.font.pointSize);
        [_vectorList setContentOffset:offset animated:YES];
    }
    
    
    //Add to magnitude list and sort.
    [_magnitudeList addObject:temp];
    [_magnitudeList sortUsingSelector:@selector(compare:)];
    
    //sets total sum and starts it if nessessary
    if(!_totalSum)
    {
        _totalSum = temp;
    }
    else
    {
        _totalSum = [_totalSum add:temp];
    }
    
    //Update Total sum label
    _sumLabel.text = [NSString stringWithFormat:@"Sum Of All Vectors: %@",_totalSum];
    
    //update longest label
    _longestLabel.text = [NSString stringWithFormat:@"Longest Vector: %@",_magnitudeList[0]];
    
    //update maginitude position label
    int magnitudePosition = _magnitudeList.count - [_magnitudeList indexOfObject:temp];
    _lastVectorLabel.text = [NSString stringWithFormat:@"Last Vector is %d of %d in Magnitude",magnitudePosition,_magnitudeList.count];
    
    //Update dot product label
    if(_vectorArray.count >1)
    {
        VECVector *v1 = [_vectorArray lastObject];
        VECVector *v2 = _vectorArray[_vectorArray.count-2];
        _productLabel.text = [NSString stringWithFormat:@"Vector %d * Vector %d: %f ",_vectorArray.count,_vectorArray.count -1,[v1 dotProduct:v2]];
    }
    
    //Play fancy sound!
    CFURLRef soundFileURLRef = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef)@"sonicRing", CFSTR ("mp3"), NULL);
    UInt32 sonicRingSound;
    AudioServicesCreateSystemSoundID(soundFileURLRef, &sonicRingSound);
    AudioServicesPlaySystemSound(sonicRingSound);
    
}


//Adds visual constraints based upon input (this is a helper method to make above code less garish.)
- (void)addVisualConstraint:(NSString *)s  withBindings:(NSDictionary *)bindings
{
    [_window.rootViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:s options:0 metrics:nil views:bindings]];
    return;
}

-(void)orientationChanged
{
    //This handles autoscrolling the scroll view.
    if(_vectorList.contentSize.height > _vectorList.bounds.size.height)
    {
        CGPoint offset = CGPointMake(0, _vectorList.contentSize.height - _vectorList.bounds.size.height-_vectorList.font.pointSize);
        [_vectorList setContentOffset:offset animated:YES];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
