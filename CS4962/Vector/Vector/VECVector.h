//
//  VECVector.h
//  Vector
//
//  Created by Adam Bradford on 1/16/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VECVector : NSObject
@property (nonatomic)double x;
@property (nonatomic)double y;
@property (nonatomic) double z;

-(VECVector *)initWithCoordinates_X:(double)x Y: (double)y Z: (double)z;
- (VECVector*) add:(VECVector*)v;
- (double) dotProduct:(VECVector*)v;
- (double) magintude;

- (NSComparisonResult)compare:(VECVector *)v;
@end
