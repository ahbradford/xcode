//
//  VECAppDelegate.h
//  Vector
//
//  Created by Adam Bradford on 1/16/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VECVector;

@interface VECAppDelegate : UIResponder <UIApplicationDelegate>
{
    UIWindow* _window;
	VECVector* _totalSum;
	UIImageView* _splashImageView;
	UILabel* _countLabelX;
    UILabel* _countLabelY;
    UILabel* _countLabelZ;
	UILabel* _sumLabel;
	UILabel* _productLabel;
	UILabel* _lastVectorLabel;
    UILabel* _longestLabel;
	UISlider* _sliderX;
    UISlider* _sliderY;
    UISlider* _sliderZ;
	UIButton* _vectorButton;
    UITextView* _vectorList;
    NSMutableArray* _magnitudeList;
    NSMutableArray* _vectorArray;
	
	NSMutableArray* _numbers;
}
@property (strong, nonatomic) UIWindow *window;

@end//comment
