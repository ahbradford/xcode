//
//  VECVector.m
//  Vector
//
//  Created by Adam Bradford on 1/16/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "VECVector.h"
@implementation VECVector
@synthesize x = _x;
@synthesize y = _y;
@synthesize z = _z;

-(VECVector *)initWithCoordinates_X:(double)x Y: (double)y Z: (double)z
{
    self = [super init];
    if(self)
    {
        _x = x;
        _y = y;
        _z = z;
    }
    
    return self;
}

- (VECVector*) add:(VECVector*)v
{
    return [[VECVector alloc]initWithCoordinates_X:self.x+v.x Y:self.y+v.y Z:self.z+v.z];
    
}

- (double) dotProduct:(VECVector*)v
{
    return (self.x*v.x) + (self.y*v.y) +(self.z*v.z);
}

- (double) magintude
{
    return sqrt(pow(self.x, 2) + pow(self.y,2) + pow(self.z, 2));
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"%.2f, %.2f, %.2f",self.x,self.y,self.z];
}
- (NSComparisonResult)compare:(VECVector *)v
{
    if([v magintude] < [self magintude]) return NSOrderedAscending;
    if([v magintude] > [self magintude]) return NSOrderedDescending;
    return NSOrderedSame;
}
@end
