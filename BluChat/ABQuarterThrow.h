//
//  ABQuarterThrow.h
//  Quarters
//
//  Created by Adam Bradford on 3/12/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABQuarterThrow : NSObject <NSCoding>

@property float accelorometerX;
@property float accelorometerY;
@property float accelorometerZ;

@property float gyroXAxisRotationRate;
@property float gyroYAxisRotationRate;
@property float gyroZAxisRotationRate;


- (void)encodeWithCoder:(NSCoder *)aCoder;
- (id)initWithCoder:(NSCoder *)aDecoder;
@end
