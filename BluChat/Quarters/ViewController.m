//
//  ViewController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ViewController.h"
#import "CentralBTController.h"
#import "PeripheralBTController.h"
#import "ABCMObserver.h"
#import "ABQuarterThrow.h"
#import <AudioToolbox/AudioToolbox.h>

@interface ViewController () <CentralBTControllerDelegate,PeripheralBTControllerDelegate,UITextFieldDelegate>
@property (strong,nonatomic) CentralBTController       *central;
@property (strong,nonatomic) PeripheralBTController    *peripheral;
@property (strong,nonatomic) ABACMObserver             *motionObserver;
@property                    SystemSoundID              sonic;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)
    {
        _peripheral = [[PeripheralBTController alloc]init];
        _peripheral.delegate = self;
        _textField.delegate = self;
    }
    else
    {
        _central = [[CentralBTController alloc]init];
        _central.delegate = self;
        _textField.delegate = self;
        [_mySwitch setOn:NO];
        
        
    }
    
    //_motionObserver = [[ABACMObserver alloc]init];
    //_motionObserver.delegate = self;
    //[_motionObserver monitorMotion];
    
    SystemSoundID sound;
    AudioServicesCreateSystemSoundID((CFURLRef)CFBridgingRetain([NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"sonicRing" ofType:@"mp3"]]), &sound);
    self.sonic = sound;
   

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dataReceivedfromPeripherial:(NSData *)data
{
    NSString *text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    text = [text stringByAppendingString:@"\n"];
    //Add description of temp to the UIScrollView
    _textView.text = [_textView.text stringByAppendingString: text];
    
    AudioServicesPlayAlertSound(self.sonic);
    
    //This handles autoscrolling the scroll view.
    if(_textView.contentSize.height > _textView.bounds.size.height)
    {
        CGPoint offset = CGPointMake(0, _textView.contentSize.height - _textView.bounds.size.height-_textView.font.pointSize);
        [_textView setContentOffset:offset animated:YES];
    }
}
-(void)dataReceivedFromCentral:(NSData *)data
{
    
    NSString *text = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    text = [text stringByAppendingString:@"\n"];
    //Add description of temp to the UIScrollView
    _textView.text = [_textView.text stringByAppendingString: text];
    
    AudioServicesPlayAlertSound(self.sonic);
    
    
    //This handles autoscrolling the scroll view.
    if(_textView.contentSize.height > _textView.bounds.size.height)
    {
        CGPoint offset = CGPointMake(0, _textView.contentSize.height - _textView.bounds.size.height-_textView.font.pointSize);
        [_textView setContentOffset:offset animated:YES];
    }
  
}

- (IBAction)switched:(UISwitch *)sender
{
    if(sender.on == NO)
    {
        _central = [[CentralBTController alloc]init];
        _central.delegate = self;
        
        if(_peripheral)[_peripheral stopAdvertising];
    
        _peripheral = nil;
    }
    else
    {
        _central = nil;
        _peripheral = [[PeripheralBTController alloc]init];
        _peripheral.delegate = self;
    }
}
- (IBAction)send
{
    if([_textField.text isEqualToString:@""])return;
    
    //Add description of temp to the UIScrollView
    NSString *message = [NSString stringWithFormat:@"%@: %@\n",_nameTextField.text, _textField.text];
    _textView.text =[ _textView.text stringByAppendingString:message];
    _textField.text = @"";
    
    if(_peripheral)
    {
        [_peripheral sendDatatoCentral:[message dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if(_central)
    {
        [_central sendDatatoPeripherial:nil data:[message dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
 
    
    //This handles autoscrolling the scroll view.
    if(_textView.contentSize.height > _textView.bounds.size.height)
    {
        CGPoint offset = CGPointMake(0, _textView.contentSize.height - _textView.bounds.size.height-_textView.font.pointSize);
        [_textView setContentOffset:offset animated:YES];
    }
}

#pragma mark UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self send];
    return YES;
}

#pragma mark ABCMObserverDelegate methods

-(void)quarterWasThrown:(ABQuarterThrow *)throw
{
    [_peripheral sendDatatoCentral:[NSKeyedArchiver archivedDataWithRootObject:throw]];
    NSLog(@"Throw sent to central");
}
@end
