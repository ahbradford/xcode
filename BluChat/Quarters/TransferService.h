//Defines UUID's


#ifndef LE_Transfer_TransferService_h
#define LE_Transfer_TransferService_h

#define TRANSFER_SERVICE_UUID                   @"BFA78515-6151-42C2-BB40-C04EE3A96F59"
#define QUARTER_THROW_CHARACTERISTIC            @"D64BE338-FA9D-4539-9A79-136786C7FB45"
#define PERIPHERIAL_SCORE_CHARACTERISTIC        @"530826F2-1A43-46A3-88B9-DC2F9D8164C6"
#define PLAYER_NAME_CHACTERISTIC                @"9A7B752B-9672-4106-9A56-1C85DE3EF57A"
#define NSLog(__FORMAT__, ...) NSLog((@"%s [Line %d] " __FORMAT__), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#endif