
//  Adam Bradford
//  CS3505
//  14 Jan 2013
//  Assignment 2 huge_number.cc
//  Interface file for huge_number class. Values will always have 0's removed from beginning of value string.

#include "huge_number.h"
#include <sstream>
    

//No argument constructor (sets value to "0")
huge_number::huge_number()
{
    this->value = "0";
}

//String arugment constructor (sets value to string)
huge_number::huge_number (string s)
{
    //remove any 0's from beginning of number
    while (s[0] == '0')
        s.erase(0, 1);
    
    // If the string is empty, it is a 0.
    
    if (s.length() == 0)
        s = "0";

    this->value = s;
}

//copy constructor
huge_number::huge_number (const huge_number& number)
{
     //remove any 0's from beginning of number
     string s = number.value;
     while (s[0] == '0')
        s.erase(0, 1);
    
    // If the string is empty, it is a 0.
    
    if (s.length() == 0)
        s = "0";

    this->value = s;
    
}


//getter for value parameter   
string huge_number:: get_value()
{
     return value;
}

//returns true if the strings are the same value.
bool areEqual (string a, string b)
{
    //casde of b being bigger than a
    if(a.length() != b.length()) return false;
    
    //iterates through each string, and returns false if values are not equal.
    int currentPosition = (int)a.length() -1;
    while(currentPosition >=0)
    {
        if((int)(a[currentPosition] - '0') != (int)(b[currentPosition] - '0')) return false;
        currentPosition--;
    }

    //numbers must be equal.
    return true;
    
}

//Adds two huge_numbers.
string add (string a,string b)
{
    // Build up a string object to contain the result.
    
    string result = "";
    
    // Work right to left.  (Most significant digits to least)
    
    int a_pos = (int)a.length() - 1;
    int b_pos = (int)b.length() - 1;
    
    int carry = 0;
    
    // Loop, adding columns, until no more columns and no carry.
    
    while (a_pos >= 0 || b_pos >= 0 || carry > 0)
    {
        // Get next digit from each string, or 0 if no more.
        
        int a_digit = a_pos >= 0 ? a[a_pos--]-'0' : 0;
        int b_digit = b_pos >= 0 ? b[b_pos--]-'0' : 0;
        
        /// Calculate the digit for this column and the carry out
        
        int sum = a_digit + b_digit + carry;
        carry = sum / 10;
        sum = sum % 10 + '0';
        
        // Put this column's digit at the start of the result string.
        
        result.insert(0, 1, static_cast<char>(sum));
    }
    
    // Strip any leading 0's  (shouldn't be any, but you'll use this elsewhere.)
    
    while (result[0] == '0')
        result.erase(0, 1);
        
        // If the string is empty, it is a 0.
        
        if (result.length() == 0)
            result = "0";
            
            // Return the result.
            
            return result;
}


//Multiplys two huge_numbers
string multiply (string a,string b)
{
    string result = "0";
    
    int b_pos = (int)b.length() - 1;
    
    // Loop once for each digit in b.
    while (b_pos >= 0)
    {
        int b_digit = b[b_pos--]-'0';  // Get next digit from b.
        
        // Add a to the result the appropriate number of times.
        
        for (int i = 0; i < b_digit; i++)
        {
            result = add(result, a);
            // cout << "  " << result << endl;  // Debug only
        }
        
        // Multiply a by 10.
        
        a.append("0");
        
        // Debug only.  (Useful to have string numbers for this!)
        // cout << a << endl;
    }
     while (result[0] == '0')
        result.erase(0, 1);
        
        // If the string is empty, it is a 0.
        
        if (result.length() == 0)
            result = "0";
    
    return result;
}

//returns true if string a is greater than string b
bool isGreaterThan( string a, string b)
{
    //case of a being larger than b	
    if(a.length() > b.length()) return true; 

    //case of b being larger than a
    else if(a.length() < b.length()) return false;
    
    //iterates through each character, testing if one is larger than the other and returning appropropriately.
    int currentPosition = 0;
    while(currentPosition <a.length())
    {
        if((int)(a[currentPosition] - '0') > (int)(b[currentPosition] - '0')) return true;
        else if((int)(a[currentPosition] - '0') < (int)(b[currentPosition] - '0')) return false;
        currentPosition++;
    }
    //numbers must be equal.
     return false;
    
    
}

//subtracts two huge_numbers
string subtract (string a,string b)
{
    // Work right to left.  (Most significant digits to least)
    
    //case of b being greater than a
    if(isGreaterThan(b, a)) return "0";
    
    //case of b smaller than a
    string result = "";
    
    int a_pos = (int)a.length() - 1;
    int b_pos = (int)b.length() - 1;
    
    int carry = 0;
    
    //loop through each character
    while (a_pos >= 0)
    {
        // Get next digit from each string, or 0 if no more.
        
        int a_digit = a_pos >= 0 ? a[a_pos--]-'0' : 0;
        int b_digit = b_pos >= 0 ? b[b_pos--]-'0' : 0;
        
        /// Calculate the digit for this column and the carry out and difference
        
        int difference = (a_digit -carry) - b_digit;
        if(difference < 0)
        {
            carry = 1;
            difference += 10;
        }
        else
        {
            carry = 0;
        }
        difference = difference + '0';
        
        // Put this column's digit at the start of the result string.
        
        result.insert(0, 1, static_cast<char>(difference));
    }
    
    // Strip any leading 0's  (shouldn't be any, but you'll use this elsewhere.)
    
    while (result[0] == '0')
        result.erase(0, 1);
    
    // If the string is empty, it is a 0.
    
    if (result.length() == 0)
        result = "0";
    
    // Return the result.
    
    return result;
}

//divides two huge_numbers using integer division.
string divide (string a,string b)
{
    //case of b greater than a
    if(isGreaterThan(b, a))return "0";
    //case of a == b

    if(areEqual(a,b)) return "1";

    //case of b == 1
    if(areEqual(b, "1"))return a;

    //case of a < b
    //set up variables.
    string quotient,remainder,temp;
    quotient = "";
    remainder = "";
    temp = "";
    int a_pos = 0;
    
    //iterate through each character
    while (a_pos < a.length())
    {
	//add character from a to temp.
        temp += a[a_pos];
	
	//append 0 if b is greater than temp and return to start of loop.
        if(isGreaterThan(b, temp))
        {
            quotient.append("0");
            a_pos++;
            continue;
        }
	
	//iterate i and check if i*b > temp if bigger than 
        string i = "0";
        string numberToTest;
        
	//iterate through each value and exit loop when value of i+1 is found
        while(isGreaterThan(temp, numberToTest) && quotient.length() <= a.length())
        {
            i = add(i,"1");
            numberToTest = multiply(b, i);
        }

	//i = i-1
        i = subtract(i, "1");
        numberToTest = subtract(numberToTest, b);
        
	//alter quotient and remainder variables approproiately.
        quotient.append(i);
        remainder = (subtract(temp, numberToTest));
        temp = remainder;
        a_pos++;

    }

    // Strip any leading 0's  (shouldn't be any, but you'll use this elsewhere.)
    
    while (quotient[0] == '0')
        quotient.erase(0, 1);

        
        if (quotient.length() == 0)
            quotient = "0";
    
    return quotient;
}


string mod (string a,string b)
{
    
    //case of b >a
    if(isGreaterThan(b, a))return b;

    //case of a == b
    if(areEqual(a,b)) return "0";

    //case of b == 1;
    if(areEqual(b, "1"))return "0";

    //set up local variables
    string result,quotient, remainder;

    //divides numbers, then multiplies quotient with b, and subtract that from a to find remainder.
    quotient = divide(a, b);
    remainder =  subtract(a, multiply(quotient, b));
    
    while (remainder[0] == '0')
        remainder.erase(0, 1);

    if(remainder.length() == 0)
	remainder = "0";
    
    return remainder;
}
        
//overload for +=
huge_number& huge_number::operator+=(const huge_number& rhs)
{
   this->value = add(value, rhs.value);
    return *this;
}

//overload for -=
huge_number& huge_number::operator-=(const huge_number& rhs)
{
    this->value = subtract(value, rhs.value);
    return *this;
}

//overload for *=
huge_number& huge_number::operator*=(const huge_number& rhs)
{
    this->value = multiply(value, rhs.value);
    return *this;
}

//overload for ==
bool huge_number::operator==(const huge_number& rhs)
{
    return areEqual(value,rhs.value);
}

//overload for =
huge_number & huge_number::operator=(const huge_number &rhs)
{
    this->value = rhs.value;
    return *this;
}

//overload for -
huge_number huge_number::operator-(const huge_number &rhs)
{
    huge_number temp = huge_number(rhs);
    temp.value = subtract(value, rhs.value);
    return temp;
}

//overload for +
huge_number huge_number::operator+(const huge_number &rhs)
{
    huge_number temp = huge_number(rhs);
    temp.value = add(value, rhs.value);
    return temp;
}

//overload for /
huge_number huge_number::operator/(const huge_number &rhs)
{
    huge_number temp = huge_number(rhs);
    temp.value = divide(value, rhs.value);
    return temp;
}

//overload for %
huge_number  huge_number::operator%(const huge_number &rhs)
{
    huge_number temp = huge_number(rhs);
    temp.value = mod(value, rhs.value);
    return temp;
}

//overload for *
huge_number & huge_number::operator*(const huge_number &rhs)
{
    huge_number *temp = new huge_number(rhs);
    temp->value = multiply(value, rhs.value);
    return *temp;
}

//friend function for cout.
std::ostream& operator<< (std::ostream &out, const huge_number &n)
{
    out << n.value;
    return out;
}

