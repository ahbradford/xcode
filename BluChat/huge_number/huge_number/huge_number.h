
//  Adam Bradford
//  CS3505
//  14 Jan 2013
//  Assignment 2 huge_number.h
//  Interface file for huge_number class. Values will always have 0's removed from beginning of value string.

#ifndef __huge_number__huge_number__
#define __huge_number__huge_number__

#include <iostream>
#include <string>


using namespace std;

class huge_number
{
public:
    
    // constructors
    huge_number();
    huge_number (string s);
    huge_number (const huge_number& number);
    
    //getter for instance variable
    string get_value();
    

    //overload operators
    huge_number & operator=(const huge_number &rhs);
    huge_number operator-(const huge_number &rhs);
    huge_number operator+(const huge_number &rhs);
    huge_number operator/(const huge_number &rhs);
    huge_number operator%(const huge_number &rhs);
    huge_number & operator*(const huge_number &rhs);
    huge_number & operator+=(const huge_number& rhs);
    huge_number & operator-=(const huge_number& rhs);
    huge_number & operator*=(const huge_number& rhs);
    bool operator==(const huge_number& rhs);

    //cout friend function
    friend std::ostream& operator<< (std::ostream &out, const huge_number &n);
    
    
     private:
    	string value;
    
};


#endif /* defined(__huge_number__huge_number__) */
