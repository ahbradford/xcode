//
//  PeripheralBTController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "PeripheralBTController.h"

#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"


@interface PeripheralBTController () <CBPeripheralManagerDelegate>
@property (strong, nonatomic) CBPeripheralManager       *peripheralManager;
@property (strong, nonatomic) CBMutableCharacteristic   *quarterThrowCharacteristic;
@property (strong, nonatomic) CBMutableCharacteristic   *scoreCharacteristic;
@property (strong, nonatomic) CBMutableCharacteristic   *playerNameCharacteristic;
@property (nonatomic) BOOL                              isConnected;
@property (nonatomic) BOOL                              sendingEOM;
@property (strong,nonatomic) NSData                     *dataToSend;
@property (nonatomic) NSInteger                         sendDataIndex;


@end

#define NOTIFY_MTU      20

@implementation PeripheralBTController

-(id)init
{
    self = [super init];
    if(self)
        
    {
        _peripheralManager = [[CBPeripheralManager alloc]initWithDelegate:self queue:nil];
        _isConnected = NO;
    }
    return self;
}

#pragma mark - CBPeripheralManagerDelegate Methods

/** Required protocol method.  A full app should take care of all the possible states,
 *  but we're just waiting for  to know when the CBPeripheralManager is ready
 */
- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    // Opt out from any other state
    if (peripheral.state != CBPeripheralManagerStatePoweredOn) {
        return;
    }
    
    // We're in CBPeripheralManagerStatePoweredOn state...
    NSLog(@"self.peripheralManager powered on.");
    
    // ... so build our service.
    
    // Start with the CBMutableCharacteristic
    _quarterThrowCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:QUARTER_THROW_CHARACTERISTIC]
                                                                     properties:CBCharacteristicPropertyNotify
                                                                          value:0
                                                                    permissions:CBAttributePermissionsReadable];
    
    // Start with the CBMutableCharacteristic
    _scoreCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:PERIPHERIAL_SCORE_CHARACTERISTIC]
                                                                     properties:CBCharacteristicPropertyWrite
                                                                          value:nil
                                                                    permissions:CBAttributePermissionsWriteable];
    
    _playerNameCharacteristic = [[CBMutableCharacteristic alloc] initWithType:[CBUUID UUIDWithString:PLAYER_NAME_CHACTERISTIC]
                                                                     properties:CBCharacteristicPropertyNotify
                                                                          value:0
                                                                    permissions:CBAttributePermissionsReadable];
    
    
    // Then the service
    CBMutableService *transferService = [[CBMutableService alloc] initWithType:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]
                                                                       primary:YES];
    
    
    // Add the characteristic to the service
    transferService.characteristics = @[_quarterThrowCharacteristic,_scoreCharacteristic,_playerNameCharacteristic];
    
    
    // And add it to the peripheral manager
    [self.peripheralManager addService:transferService];
    
    [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] }];
    
    
    
}


- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didSubscribeToCharacteristic:(CBCharacteristic *)characteristic
{
    NSLog(@"Central subscribed to characteristic");
    
    
    
    // Start sending
    
    
    [self stopAdvertising];
    _isConnected = YES;
}


- (void)peripheralManagerDidStartAdvertising:(CBPeripheralManager *)peripheral error:(NSError *)error
{

}
/** Recognise when the central unsubscribes
 */
- (void)peripheralManager:(CBPeripheralManager *)peripheral central:(CBCentral *)central didUnsubscribeFromCharacteristic:(CBCharacteristic *)characteristic
{
    [self.peripheralManager startAdvertising:@{ CBAdvertisementDataServiceUUIDsKey : @[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]] }];
    NSLog(@"Central unsubscribed from characteristic");
}

/** Sends the next amount of data to the connected central
 */
- (void)sendData
{
    // First up, check if we're meant to be sending an EOM
    static BOOL sendingEOM = NO;
    
    if (sendingEOM) {
        
        // send it
        BOOL didSend = [self.peripheralManager updateValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:self.quarterThrowCharacteristic onSubscribedCentrals:nil];
        
        // Did it send?
        if (didSend) {
            
            // It did, so mark it as sent
            sendingEOM = NO;
        
            _sendDataIndex = 0;
            NSLog(@"Sent: EOM");
            
        }
        
        // It didn't send, so we'll exit and wait for peripheralManagerIsReadyToUpdateSubscribers to call sendData again
        return;
    }
    
    // We're not sending an EOM, so we're sending data
    
    // Is there any left to send?
    
    if (self.sendDataIndex >= self.dataToSend.length) {
        
        // No data left.  Do nothing
        return;
    }
    
    // There's data left, so send until the callback fails, or we're done.
    
    BOOL didSend = YES;
    
    while (didSend) {
        
        // Make the next chunk
        
        // Work out how big it should be
        NSInteger amountToSend = self.dataToSend.length - self.sendDataIndex;
        
        // Can't be longer than 20 bytes
        if (amountToSend > NOTIFY_MTU) amountToSend = NOTIFY_MTU;
        
        // Copy out the data we want
        NSData *chunk = [NSData dataWithBytes:self.dataToSend.bytes+self.sendDataIndex length:amountToSend];
        
        // Send it
        didSend = [_peripheralManager updateValue:chunk forCharacteristic:_quarterThrowCharacteristic onSubscribedCentrals:nil];
        
        // If it didn't work, drop out and wait for the callback
        if (!didSend) {
            return;
        }
        
       
        //NSLog(@"Sent: %@", stringFromData);
        
        // It did send, so update our index
        self.sendDataIndex += amountToSend;
        
        // Was it the last one?
        if (self.sendDataIndex >= self.dataToSend.length) {
            
            // It was - send an EOM
            
            // Set this so if the send fails, we'll send it next time
            sendingEOM = YES;
            
            // Send it
            BOOL eomSent = [self.peripheralManager updateValue:[@"EOM" dataUsingEncoding:NSUTF8StringEncoding] forCharacteristic:self.quarterThrowCharacteristic onSubscribedCentrals:nil];
            
            if (eomSent) {
                // It sent, we're all done
                sendingEOM = NO;
                
                NSLog(@"Sent: EOM");
                _sendDataIndex = 0;
            }
            
            return;
        }
    }
}


/** This callback comes in when the PeripheralManager is ready to send the next chunk of data.
 *  This is to ensure that packets will arrive in the order they are sent
 */
- (void)peripheralManagerIsReadyToUpdateSubscribers:(CBPeripheralManager *)peripheral
{
    // Start sending again
    [self sendData];
}

-(void)peripheralManager:(CBPeripheralManager *)peripheral didReceiveWriteRequests:(NSArray *)requests
{
    //CBATTRequest *request = requests[0];
    //if([request.characteristic.UUID.description isEqualToString:PERIPHERIAL_SCORE_CHARACTERISTIC])
    NSString *response = [[NSString alloc]init];
    for(CBATTRequest *request in requests)
    {
        NSString * tempString = [[NSString alloc]initWithData:request.value encoding:NSUTF8StringEncoding];
        response = [response stringByAppendingString:tempString];
    }
    _scoreCharacteristic.value = [response dataUsingEncoding:NSUTF8StringEncoding];
    [_delegate dataReceivedFromCentral:_scoreCharacteristic.value];
    

}


#pragma mark PeripheralBTController methods
-(void)stopAdvertising
{
    [_peripheralManager stopAdvertising];
}

-(void)sendDatatoCentral:(NSData *)d
  
{
    self.dataToSend = d;
    [self sendData];
}


@end
