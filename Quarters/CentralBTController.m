//
//  CentralBTController.m
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <CoreBluetooth/CoreBluetooth.h>
#import "TransferService.h"
#import "CentralBTController.h"

@interface CentralBTController () <CBCentralManagerDelegate, CBPeripheralDelegate>


@property (strong, nonatomic) CBCentralManager      *centralManager;
@property (strong, nonatomic) NSMutableData         *data;
@property (strong, nonatomic) NSMutableSet          *peripherialSet;
@property (strong)            NSMutableDictionary   *currentReceivesinProgress;
@property                       CBCharacteristic   *characteristic;
@end

@implementation CentralBTController



-(id)init
{
    self = [super init];
    if(self)
    {
        _centralManager = [[CBCentralManager alloc]initWithDelegate:self queue:nil];
        _peripherialSet = [[NSMutableSet alloc]init];
        _currentReceivesinProgress = [[NSMutableDictionary alloc]init];
    }
    return self;
}

#pragma mark CBCentralManagerDelegate methods

//check update state, we are waiting for powered on state, if the device does not support BTLE then nothing
//will happen.
-(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if(central.state == CBCentralManagerStateUnsupported) return;
    if(central.state == CBCentralManagerStatePoweredOn)
    {
        [self scan];
    }
}

//Scan for peripherals that are using our UUID.
-(void)scan
{
    [_centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]
                                            options:@{CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
    NSLog(@"Scanning Started");
}

/** This callback comes whenever a peripheral that is advertising the TRANSFER_SERVICE_UUID is discovered.
 *  We check the RSSI, to make sure it's close enough that we're interested in it, and if it is,
 *  we start the connection process
 */

//Called when a peripherail that is advertising our UUID is discovered, it is then check to see roughy how close it is based
//on power, then we wil connect to it.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    // Reject any where the value is above reasonable range
    if (RSSI.integerValue > -15) {
        return;
    }
    
    // Reject if the signal strength is too low to be close enough (Close is around -22dB)
    if (RSSI.integerValue < -35) {
        return;
    }
    
    NSLog(@"Discovered %@ at %@", peripheral.name, RSSI);
    
    // Ok, it's in range - have we already seen it?
    if (![_peripherialSet containsObject:peripheral]) {
        
        // Save a local copy of the peripheral, so CoreBluetooth doesn't get rid of it
        [_peripherialSet addObject :peripheral];
        
        // And connect
        NSLog(@"Connecting to peripheral %@", peripheral);
        [_centralManager connectPeripheral:peripheral options:nil];
    }
}

//Deals with a failed connection
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Failed to connect to %@. (%@)", peripheral, [error localizedDescription]);
    [self cleanupPeripherial:peripheral];
}

/** We've connected to the peripheral, now we need to discover the services and characteristics to find the 'transfer' characteristic.
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Peripheral Connected");
    

    // Make sure we get the discovery callbacks
    peripheral.delegate = self;
    
    // Search only for services that match our UUID
    [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]];
}

/** The Transfer Service was discovered
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [self cleanupPeripherial:peripheral];
        return;
    }

    // Discover the characteristic we want...
    
    // Loop through the newly filled peripheral.services array, just in case there's more than one.
    for (CBService *service in peripheral.services) {
        [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:QUARTER_THROW_CHARACTERISTIC]] forService:service];
    }
    
}


/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanupPeripherial:peripheral];
        return;
    }
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        
        // And check if it's the right one
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:QUARTER_THROW_CHARACTERISTIC]])
        {
            
            // If it is, subscribe to it
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            [_peripherialSet addObject:peripheral];
        }
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PERIPHERIAL_SCORE_CHARACTERISTIC]])
        {
            
            // If it is, subscribe to it
           // [peripheral setNotifyValue:YES forCharacteristic:characteristic];
          //  [_peripherialSet addObject:peripheral];
            _characteristic = characteristic;
        }
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:PLAYER_NAME_CHACTERISTIC]])
        {
            
            // If it is, subscribe to it
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            _characteristic = characteristic;
        }
    }
    
    // Once this is complete, we just need to wait for the data to come in.
}

/** This callback lets us know more data has arrived via notification on the characteristic
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual: [CBUUID UUIDWithString: QUARTER_THROW_CHARACTERISTIC]])
    {
        if (error)
        {
            NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
            return;
        }
        
        NSMutableData *receivedData = _currentReceivesinProgress[[NSValue valueWithPointer:(__bridge const void *)(peripheral)]];
        
        
        NSString *temp = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        
        if(!receivedData)
        {
            receivedData = [[NSMutableData alloc]init];
            [_currentReceivesinProgress setObject:receivedData forKey:[NSValue valueWithPointer:(__bridge const void *)(peripheral)]];
        }
        
        temp = [[NSString alloc]initWithData:receivedData encoding:NSUTF8StringEncoding];
        
        NSString *stringFromData = [[NSString alloc] initWithData:characteristic.value encoding:NSUTF8StringEncoding];
        
        // Have we got everything we need?
        if ([stringFromData isEqualToString:@"EOM"])
        {
            NSLog(@"Received: %@", stringFromData);
            [_delegate dataReceivedfromPeripherial:receivedData];
            [_currentReceivesinProgress removeObjectForKey:[NSValue valueWithPointer:(__bridge const void *)(peripheral)]];
        }
        
        // Otherwise, just add the data on to what we already have
        [receivedData appendData:characteristic.value];
    }
    
    
}

-(void)sendDatatoPeripherial:(CBPeripheral *)peripheral data:(NSData*)data
{
    CBPeripheral *p = (CBPeripheral*)[_peripherialSet anyObject];
    if(p)
    { 
   [p writeValue:data forCharacteristic:_characteristic type:CBCharacteristicWriteWithResponse];
    }
    
}
-(void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if(error)
        NSLog(@"%@",[error localizedDescription]);
}





/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Exit if it's not the transfer characteristic
    if (!([characteristic.UUID isEqual:[CBUUID UUIDWithString:QUARTER_THROW_CHARACTERISTIC]]||[characteristic.UUID isEqual:[CBUUID UUIDWithString:PERIPHERIAL_SCORE_CHARACTERISTIC]] )) {
        return;
    }
    
    // Notification has started
    if (characteristic.isNotifying) {
        NSLog(@"Notification began on %@", characteristic);
    }
    
    // Notification has stopped
    else {
        // so disconnect from the peripheral
        NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        [self.centralManager cancelPeripheralConnection:peripheral];
    }
}


/** Once the disconnection happens, we need to clean up our local copy of the peripheral
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Peripheral Disconnected");
    [_peripherialSet removeObject:peripheral];
    
    // We're disconnected, so start scanning again
    [self scan];
}

/** Call this when things either go wrong, or you're done with the connection.
 *  This cancels any subscriptions if there are any, or straight disconnects if not.
 *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
 */
- (void)cleanupPeripherial:(CBPeripheral *)peripheral
{
    // Don't do anything if we're not connected
    if (peripheral.isConnected)
    {
        return;
    }
    
    // See if we are subscribed to a characteristic on the peripheral
    if (peripheral.services != nil)
    {
        for (CBService *service in peripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:QUARTER_THROW_CHARACTERISTIC]]) {
                        if (characteristic.isNotifying) {
                            // It is notifying, so unsubscribe
                            [peripheral setNotifyValue:NO forCharacteristic:characteristic];
                            
                            // And we're done.
                            return;
                        }
                    }
                }
            }
        }
    }
    
    // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
    [self.centralManager cancelPeripheralConnection:peripheral];
}



@end