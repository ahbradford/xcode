//
//  ABAccelorometerObserver.h
//  Quarters
//
//  Created by Adam Bradford on 3/12/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>
@class ABQuarterThrow;

@protocol ABCMObserverDelegate <NSObject>
-(void)quarterWasThrown:(ABQuarterThrow *)throw;

@end

@interface ABACMObserver : NSObject;

@property (weak,nonatomic) id<ABCMObserverDelegate> delegate;
@property NSInteger triggerTheasholdRate; // on the Z axis.  default is 2.75 (G's)


-(void)monitorMotion;
@end
