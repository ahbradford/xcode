//
//  ViewController.h
//  Quarters
//
//  Created by Adam Bradford on 3/11/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABCMObserver.h"

@interface ViewController : UIViewController <ABCMObserverDelegate>
- (IBAction)switched:(UISwitch *)sender;

@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)send;

@end
