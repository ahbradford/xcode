//
//  CruncherAppDelegate.m
//  Cruncher
//
//  Created by Matt on 1/19/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import "CruncherAppDelegate.h"

#pragma mark -
@implementation CruncherAppDelegate

#pragma mark Constructors
- (void) applicationDidFinishLaunching:(UIApplication*)application 
{   
    //Create window
    _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	[_window setBackgroundColor:[UIColor grayColor]];
    [_window makeKeyAndVisible];
    
    UIViewController* viewController = [[UIViewController alloc] init];
    [_window setRootViewController:viewController];
	
	// Create decorative splash image
	_splashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 20.0f, 320.0f, 200.0f)];
	[_splashImageView setImage:[UIImage imageNamed:@"torus.png"]];
	[_splashImageView setContentMode:UIViewContentModeScaleAspectFit];
	[viewController.view addSubview:_splashImageView];
	
	// Create data labels
	_countLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 250.0f, 320.0f, 30.0f)];
	[_countLabel setBackgroundColor:[UIColor clearColor]];
	[_countLabel setText:@"Count"];
	[_countLabel setTextAlignment:NSTextAlignmentCenter];
	[viewController.view addSubview:_countLabel];
	
	_sumLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 280.0f, 320.0f, 30.0f)];
	[_sumLabel setBackgroundColor:[UIColor clearColor]];
	[_sumLabel setText:@"Sum"];
	[_sumLabel setTextAlignment:NSTextAlignmentCenter];
	[viewController.view addSubview:_sumLabel];
	
	_productLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 310.0f, 320.0f, 30.0f)];
	[_productLabel setBackgroundColor:[UIColor clearColor]];
	[_productLabel setText:@"Product"];
	[_productLabel setTextAlignment:NSTextAlignmentCenter];
	[viewController.view addSubview:_productLabel];
	
	_positionOfLastElementLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 340.0f, 320.0f, 30.0f)];
	[_positionOfLastElementLabel setBackgroundColor:[UIColor clearColor]];
	[_positionOfLastElementLabel setText:@"Position in array"];
	[_positionOfLastElementLabel setTextAlignment:NSTextAlignmentCenter];
	[viewController.view addSubview:_positionOfLastElementLabel];
	
	// Create data input slider
	_slider = [[UISlider alloc] initWithFrame:CGRectMake(10.0f, 400.0f, 300.0f, 20.0f)];
	[_slider setMinimumValue:-100.0f];
	[_slider setMaximumValue:100.0f];
	[_slider setValue:0.0f];
	[viewController.view addSubview:_slider];
	
	// Create execute button
	_crunchButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
	[_crunchButton setFrame:CGRectMake(10.0f, 430.0f, 300.0f, 30.0f)];
	[_crunchButton setTitle:@"Crunch Data!" forState:UIControlStateNormal];
	[_crunchButton addTarget:self 
		action:@selector(crunchPressed) 
		forControlEvents:UIControlEventTouchUpInside];
	[viewController.view addSubview:_crunchButton];
	
	// Create an empty collection to store the data input by the user
	_numbers = [[NSMutableArray alloc] init];
}

- (void) applicationWillTerminate:(UIApplication*)application
{
    [_window release];
}

- (void) crunchPressed
{
	// Read the value of the data slider and convert it to a number object suitable to add to the data collection
	float sliderValue = [_slider value];
	NSNumber* sliderValueNumber = [NSNumber numberWithFloat:sliderValue];
	[_numbers addObject:sliderValueNumber];
	
	// Get the number of elements in the data collection, format it as a string, then set that string as the text of the count label
	[_countLabel setText:[NSString stringWithFormat:@"Count: %i", [_numbers count]]];
	
	// Calculate the sum of the data elements and format it for presentation in the sum label
	float sum = 0.0f;
	for (int i = 0; i < [_numbers count]; i++)
	{
		NSNumber* element = [_numbers objectAtIndex:i];
		float value = [element floatValue];
		sum += value;
	}
	NSString* sumText = [NSString stringWithFormat:@"Sum: %f", sum];
	[_sumLabel setText:sumText];

	// Calculate the product of the data elements and format it for presentation in the product label
	float product = 1.0f;
	for (int i = 0; i < [_numbers count]; i++)
	{
		NSNumber* element = [_numbers objectAtIndex:i];
		float value = [element floatValue];
		product *= value;
	}
	NSString* productText = [NSString stringWithFormat:@"Product: %f", product];
	[_productLabel setText:productText];
	
	// Sort the data and determine the location of the most recently input data in the sorted collection
	NSArray* sortedNumbers = [_numbers sortedArrayUsingSelector:@selector(compare:)];
	NSUInteger lastNumberIndex = [sortedNumbers indexOfObject:sliderValueNumber];
	NSString* string = [NSString stringWithFormat:@"Position in array: %i", lastNumberIndex];
	[_positionOfLastElementLabel setText:string];
	
	// Have some fun with the splash image using the CoreAnimation framework exposed through the UIView class
	[UIView beginAnimations:nil context:nil];
	[_splashImageView setTransform:CGAffineTransformRotate([_splashImageView transform], M_PI / 6.0f)];
	[UIView commitAnimations];
}

@end
