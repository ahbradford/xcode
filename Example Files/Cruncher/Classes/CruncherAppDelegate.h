//
//  CruncherAppDelegate.h
//  Cruncher
//
//  Created by Matt on 1/19/11.
//  Copyright __MyCompanyName__ 2011. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CruncherAppDelegate : NSObject <UIApplicationDelegate> 
{
    UIWindow* _window;
	
	UIImageView* _splashImageView;
	UILabel* _countLabel;
	UILabel* _sumLabel;
	UILabel* _productLabel;
	UILabel* _positionOfLastElementLabel;
	UISlider* _slider;
	UIButton* _crunchButton;
	
	NSMutableArray* _numbers;
}

@end

