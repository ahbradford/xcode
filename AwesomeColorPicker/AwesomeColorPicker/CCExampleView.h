//
//  CCExampleView.h
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCExampleView : UIView
{
    UILabel* _currentColorLabel;
    UILabel* _previousColorLabel;
    UIButton* _currentColorButton;
    UIButton* _previousColorButton;
}

@end
