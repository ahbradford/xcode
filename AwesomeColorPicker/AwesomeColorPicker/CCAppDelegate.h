//
//  CCAppDelegate.h
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
