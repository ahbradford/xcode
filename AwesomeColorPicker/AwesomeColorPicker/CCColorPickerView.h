//
//  CCColorPickerView.h
//  AwesomeColorPicker
//
//  Created by __YOUR_NAME_HERE__ on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCColorPickerProtocol.h"

// Your custom color picker should inherit from UIControl and conform to the protocol CCColorPickerProtocol
// If you do not implement the ColorPickerView here, be sure to import the CCColorPickerProtocol into your project
// and be sure your UIControl subclass conforms to CCColorPickerProtocol.
@interface CCColorPickerView : UIControl <CCColorPickerProtocol>

@end
