//
//  CCColorPickerView.m
//  AwesomeColorPicker
//
//  Created by __YOUR_NAME_HERE__ on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import "CCColorPickerView.h"

@implementation CCColorPickerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        // Add any subviews or sub-controls here
        [self setBackgroundColor:[UIColor underPageBackgroundColor]];
        
    }
    return self;
}

#pragma mark - CCColorPickerProtocol methods
/**
 * Set the current color of the Color chooser
 */
- (void) setColor:(UIColor*)color
{
    
}

/**
 * Get the current color from the Color chooser
 */
- (UIColor*) color
{
    return [UIColor blueColor];
}

/**
 * Set the previous color of the Color chooser
 */
- (void) setPreviousColor:(UIColor*)color
{
    
}

/**
 * Get the previous color from the Color chooser
 */
- (UIColor*) previousColor
{
    return [UIColor greenColor];
}

#pragma mark - CCColorPickerView methods
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
