//
//  CCExampleView.m
//  AwesomeColorPicker
//
//  Created by Kevin Wong on 1/29/13.
//  Copyright (c) 2013 cs4962. All rights reserved.
//

#import "CCExampleView.h"
#import "CCColorPickerProtocol.h"
#import "CCColorPickerView.h"
#import "ABColorPicker.h"

#define CCExampleViewInsetAmount 10.0f

@implementation CCExampleView
{
    ABColorPicker* _colorPicker;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        // ********************************************
        // *** Create your custom color picker here ***
        // ********************************************

        // Create color picker view. Perform any initialization of the color picker as well.
        _colorPicker = [[ABColorPicker alloc] init];
        [self addSubview:_colorPicker];
        
        
        
        
        
        // Validate that the color picker exists and conforms to the protocol
        // If you get a
        [_colorPicker addTarget:self action:@selector(colorChanged:) forControlEvents:UIControlEventValueChanged];
        if (_colorPicker == nil)
            NSLog(@"ERROR: Color Picker does not exist.");
        
        if ([_colorPicker isKindOfClass:[UIControl class]] == FALSE)
            NSLog(@"ERROR: Color Picker must be a sub-class of UIControl");
             
        if ([_colorPicker conformsToProtocol:@protocol(CCColorPickerProtocol)] == FALSE)
            NSLog(@"ERROR: Color picker does not conform to the CCColorPickerProtocol");
        
        // Create two labels to identify the color swatches
        _currentColorLabel = [[UILabel alloc] init];
        [_currentColorLabel setText:@"Current Color"];
        [_currentColorLabel setBackgroundColor:[UIColor clearColor]];
        [_currentColorLabel setTextColor:[UIColor whiteColor]];
        [_currentColorLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_currentColorLabel];

        _previousColorLabel = [[UILabel alloc] init];
        [_previousColorLabel setText:@"Previous Color"];
        [_previousColorLabel setBackgroundColor:[UIColor clearColor]];
        [_previousColorLabel setTextColor:[UIColor whiteColor]];
        [_previousColorLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_previousColorLabel];
        
        // Create a current color and previous color swatch. When they are pressed, they set the color of the colorPicker to a random color
        _currentColorButton = [[UIButton alloc] init];
        [_currentColorButton setBackgroundColor:[_colorPicker color]];
        [_currentColorButton addTarget:self action:@selector(colorButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_currentColorButton];
        
        _previousColorButton = [[UIButton alloc] init];
        [_previousColorButton setBackgroundColor:[_colorPicker previousColor]];
        [_previousColorButton addTarget:self action:@selector(colorButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_previousColorButton];
    }
    return self;
}

- (void) colorChanged:(id)sender
{
    // Update the color swatches when the color gets changed
    [_currentColorButton setBackgroundColor:[_colorPicker color]];
    [_previousColorButton setBackgroundColor:[_colorPicker previousColor]];
}

- (void) colorButtonPressed:(id)sender
{
    // Create a random color
    float hue = (float) arc4random() / RAND_MAX;
    float saturation = (float) arc4random() / RAND_MAX;
    float brightness = (float) arc4random() / RAND_MAX;
    UIColor* randomColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1.0f];
    
    // set the randomly generate color for the color picker
    if (sender == _currentColorButton)
    {
        [_colorPicker setColor:randomColor];
    }
    else if (sender == _previousColorButton)
    {
        [_colorPicker setPreviousColor:randomColor];
    }
}

- (void) layoutSubviews
{
    // Get the bounds of the current view. We will use this to dynamically calculate the frames of our subviews
    CGRect bounds = [self bounds];
    
    CGRect topRect;
    CGRect colorPickerRect;
    CGRect currentColorButtonRect;
    CGRect previousColorButtonRect;
    CGRect currentColorLabelRect;
    CGRect previousColorLabelRect;

    // Cut off 120 points for the top
    CGRectDivide(bounds, &topRect, &colorPickerRect, 120.0f, CGRectMinYEdge);

    // Split the top rect in half
    CGRectDivide(topRect, &currentColorButtonRect, &previousColorButtonRect, bounds.size.width / 2.0f, CGRectMinXEdge);
 
    // Inset these views a little bit so there is a border
    currentColorButtonRect = CGRectInset(currentColorButtonRect, CCExampleViewInsetAmount, CCExampleViewInsetAmount);
    previousColorButtonRect = CGRectInset(previousColorButtonRect, CCExampleViewInsetAmount, CCExampleViewInsetAmount);
    colorPickerRect = CGRectInset(colorPickerRect, CCExampleViewInsetAmount, CCExampleViewInsetAmount);
    
    // Cut off the top 20 pixels of the color swatches for a label
    CGRectDivide(currentColorButtonRect, &currentColorLabelRect, &currentColorButtonRect, 20.0f, CGRectMinYEdge);
    CGRectDivide(previousColorButtonRect, &previousColorLabelRect, &previousColorButtonRect, 20.0f, CGRectMinYEdge);
    
    
    // Set the frames
    [_currentColorLabel setFrame:CGRectIntegral(currentColorLabelRect)];
    [_previousColorLabel setFrame:CGRectIntegral(previousColorLabelRect)];
    [_currentColorButton setFrame:CGRectIntegral(currentColorButtonRect)];
    [_previousColorButton setFrame:CGRectIntegral(previousColorButtonRect)];
    [_colorPicker setFrame:CGRectIntegral(colorPickerRect)];
}

@end
