//
//  ABSlider.h
//  ColorPicker
//  Creates a slimple custom slider
//  Created by adambradford on 2/3/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABSlider : UIControl

@property (nonatomic) float value;

@end
