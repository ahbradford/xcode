//  ABColorCrossHair.h
//  ColorPicker
//  Draws a crossHair and circle at the CurrentLocation property.  Just set the property and it moves.
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABColorCrossHair : UIView

@property (nonatomic) CGPoint currentLocation;
@property int centerCircleSize;
@property float lineWidth;

@end
