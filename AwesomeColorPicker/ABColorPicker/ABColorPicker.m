//
//  ABColorPicker.m
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//
#import <sys/utsname.h>
#import "ABColorPicker.h"
#import "ABColorField.h"
#import "ABSlider.h"
@interface ABColorPicker() <ABColorFieldDelegate>

@property (nonatomic) NSMutableDictionary *bindings;
@property (nonatomic) ABColorField* colorField;
@property (nonatomic) UIButton *currentColor;
@property (nonatomic) UILabel *cbLabel;
@property (nonatomic) UILabel *crLabel;
@property (nonatomic) UILabel *yLabel;
@property (nonatomic) UIView *allValues;
@property (nonatomic) ABSlider *slider;
@property (nonatomic) int rectSize;

//method used to get type of device currently being used.
-(NSString *)machineName;

@end

@implementation ABColorPicker

@synthesize colorField = _colorField;
@synthesize bindings = _bindings;
@synthesize currentColor = _currentColor;
@synthesize cbLabel = _cbLabel;
@synthesize crLabel = _crLabel;
@synthesize yLabel = _yLabel;
@synthesize allValues = _allValues;
@synthesize slider = _slider;
@synthesize rectSize = _rectSize;
@synthesize color = _color;
@synthesize previousColor = _previousColor;


//sets the currently selected color
-(void)setColor:(UIColor *)acolor
{
    _colorField.color = acolor;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

//sets the previously selected color
-(void)setPreviousColor:(UIColor *)previousColor
{
    _previousColor = previousColor;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

//initialize
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _color = [UIColor greenColor];
        _previousColor = [UIColor blueColor];
        
        //get current device type, rendering the color field is painful on a slower device, but runs fast
        // on an A6 chip.
        NSString *deviceType = [self machineName];
        if([deviceType isEqualToString:@"iPhone5,1"])_rectSize = 4;
        else if([deviceType isEqualToString:@"iPhone5,2"])_rectSize = 4;
        else if([deviceType isEqualToString:@"iPad4,1"])_rectSize = 4;
        else if([deviceType isEqualToString:@"x86_64"])_rectSize = 8;
        else _rectSize = 16;
        
        //create the colorfield and current color views.
        _bindings = [[NSMutableDictionary alloc]init];
        _colorField = [[ABColorField alloc]init];
        _colorField.yValue = 128;
        _colorField.delegate = self;
        _currentColor = [[UIButton alloc]init];
        
        //create slider view
        _slider = [[ABSlider alloc]init];
        [_slider addTarget:self action:@selector(yValueChanged:) forControlEvents:UIControlEventValueChanged];
        [_slider addTarget:self action:@selector(sliderStopped) forControlEvents:UIControlEventEditingDidEnd];
        
        //set up labels
        _cbLabel = [[UILabel alloc]init];
        _cbLabel.text = @"Cb :   ";
        _cbLabel.textAlignment = NSTextAlignmentCenter;
        _cbLabel.textColor = [UIColor whiteColor];
        _cbLabel.backgroundColor = [UIColor blackColor];
        _crLabel = [[UILabel alloc]init];
        _crLabel.text = @"Cr:    ";
        _crLabel.textColor = [UIColor whiteColor];
        _crLabel.textAlignment = NSTextAlignmentRight;
        _crLabel.backgroundColor = [UIColor blackColor];
        _yLabel = [[UILabel alloc]init];
        _yLabel.text = [NSString stringWithFormat:@"Y: %d",(int)_colorField.yValue];
        _yLabel.textAlignment = NSTextAlignmentLeft;
        _yLabel.textColor = [UIColor whiteColor];
        _yLabel.backgroundColor = [UIColor blackColor];
        
        [self addSubview:_colorField];
        [self addSubview:_currentColor];
        [self addSubview:_slider];
        [self addSubview:_cbLabel];
        [self addSubview:_crLabel];
        [self addSubview:_yLabel];
        
        //set up for auto layout
        _slider.translatesAutoresizingMaskIntoConstraints = NO;
        _colorField.translatesAutoresizingMaskIntoConstraints = NO;
        _currentColor.translatesAutoresizingMaskIntoConstraints = NO;
        _yLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _crLabel.translatesAutoresizingMaskIntoConstraints = NO;
        _cbLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [_bindings addEntriesFromDictionary:NSDictionaryOfVariableBindings(_colorField,_currentColor,_cbLabel,_crLabel,_yLabel,_slider)];
        
        //add visual constraints
        [self addVisualConstraint:@"V:|[_crLabel(==20)]" withBindings:_bindings];
        [self addVisualConstraint:@"V:|[_cbLabel(==20)]" withBindings:_bindings];
        [self addVisualConstraint:@"V:|[_yLabel(==20)]-[_currentColor(==30)]-[_colorField][_slider(<=40)]|" withBindings:_bindings];
        [self addVisualConstraint:@"H:|-[_yLabel(==75)]" withBindings:_bindings];
        [self addVisualConstraint:@"H:[_crLabel(==75)]-|" withBindings:_bindings];
        [self addVisualConstraint:@"H:[_cbLabel(<=100)]" withBindings:_bindings];
        [self addVisualConstraint:@"H:|[_slider]|" withBindings:_bindings];
        [self addVisualConstraint:@"H:|[_currentColor]|" withBindings:_bindings];
        
        NSLayoutConstraint *colorFieldHeightConstraint =[NSLayoutConstraint
                                                         constraintWithItem:_colorField
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                         toItem:_colorField
                                                         attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0f
                                                         constant:0];
        
        NSLayoutConstraint *colorFieldCenterContraint = [NSLayoutConstraint
                                                         constraintWithItem:_colorField
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                         toItem:self
                                                         attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0
                                                         constant:0];
        
        NSLayoutConstraint *centerLabelContraint = [NSLayoutConstraint
                                                    constraintWithItem:_cbLabel
                                                    attribute:NSLayoutAttributeCenterX
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self
                                                    attribute:NSLayoutAttributeCenterX
                                                    multiplier:1.0
                                                    constant:0];
        
        [self addConstraint:colorFieldHeightConstraint];
        [self addConstraint:centerLabelContraint];
        [self addConstraint:colorFieldCenterContraint];
        
        self.userInteractionEnabled  = YES;
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}


//updates the selected color
-(void)updateSelectedColor:(UIColor *)acolor
{
    _currentColor.backgroundColor = acolor;
    _previousColor = _color;
    _color = _currentColor.backgroundColor ;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    [_currentColor setNeedsDisplay];
}


//handles when the Y value slider changes.
-(IBAction)yValueChanged:(UISlider *)sender
{
    _colorField.rectSize = _rectSize;
    _colorField.yValue = (int)sender.value;
    _yLabel.text = [NSString stringWithFormat:@"Y: %d",(int)sender.value];
    [_colorField setNeedsDisplay];
}


//if the slider has stopped, redraw the colorfield at a higher resolution
-(void)sliderStopped
{
    _colorField.rectSize = 4;
    [_colorField setNeedsDisplay];
}


//updates the cr label
-(void)updateCrValue:(int)cR
{
    _crLabel.text = [NSString stringWithFormat:@"Cr: %d",cR];
    _yLabel.text = [NSString stringWithFormat:@"Y: %d",(int) _colorField.yValue];
    _slider.value = _colorField.yValue;
    [_slider setNeedsDisplay];
}


//updates the Cb label
-(void)updateCbVAlue:(int)cB
{
    _cbLabel.text = [NSString stringWithFormat:@"Cb: %d",cB];
}


//returns the current divice name
-(NSString*) machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}


//Adds visual constraints based upon input (this is a helper method to make above code less garish.)
- (void)addVisualConstraint:(NSString *)s  withBindings:(NSDictionary *)bindings
{
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:s options:0 metrics:nil views:bindings]];
    return;
}

@end
