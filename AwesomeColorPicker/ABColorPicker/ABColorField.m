//
//  ABColorField.m
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABColorField.h"
#import "ABColorCrossHair.h"
#import <QuartzCore/QuartzCore.h>




//Private Properties
@interface ABColorField()

@property ABColorCrossHair *crossHair;
@property UIView *selectedColor;



@end

@implementation ABColorField

@synthesize crossHair = _crossHair;
@synthesize delegate = _delegate;
@synthesize yValue = _yValue;
@synthesize rectSize = _rectSize;
@synthesize color = _color;
@synthesize previousColor = _previousColor;


//sets the color to the current color, and updates the crosshairs.
//It will also set the yValue,cb and cr to the YCBCR equalivant of the color paramater.
-(void)setColor:(UIColor *)color
{
    CGFloat r = 0.0f, g = 0.0f, b = 0.0f;
    [color getRed:&r green:&g blue:&b alpha:0];
    r = r*255;
    g = g*255;
    b = b*255;
    CGFloat cr,cb = 0;
    
    //fancy math that converts RGB to YCBCR, inputs are 0-255 http://www.equasys.de/colorconversion.html
    _yValue = .299*r + .587*g + .114*b;
    cb = 128 + (-.169*r -.331*g + .5*b);
    cr = 128 + (.5*r -.419*g -.081*b);
    
    //update crosshair Location
    CGPoint crossHairLocation;
    crossHairLocation.x = self.frame.size.width/256*cb;
    crossHairLocation.y = self.frame.size.height/255*cr;
    _crossHair.currentLocation = crossHairLocation;
    
    [self setNeedsDisplay];
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _rectSize = 4;
        _crossHair = [[ABColorCrossHair alloc]init];
        _crossHair.translatesAutoresizingMaskIntoConstraints = NO;
        self.contentMode = UIViewContentModeRedraw;
        [self addSubview:_crossHair];
        
        //constrain the crosshair to the size of the colorField;
        [self addVisualConstraint:@"H:|[_crossHair]|" withBindings:NSDictionaryOfVariableBindings(_crossHair)];
        [self addVisualConstraint:@"V:|[_crossHair]|" withBindings:NSDictionaryOfVariableBindings(_crossHair)];
        
    }
    return self;
}


- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //This draws the color field in the current frame.
    float cb,cr = 0;
    for(int i = 0; i < self.bounds.size.width; i = i +_rectSize)
    {
        for(int j = 0; j < self.bounds.size.height; j = j + _rectSize)
        {
            cb = (i/self.bounds.size.width * 255);
            
            CGContextAddRect(context, CGRectMake(i, j, _rectSize, _rectSize));
            
            cr = 255 -(j/self.bounds.size.width * 255);
            [[self convertYCbCrToRGBwithY:_yValue Cb:cb Cr:cr]setFill];
            CGContextDrawPath(context, kCGPathFill);
        }
    }
    
    //notify delegate of new cbcr values
    [self calcCbCrandCallbackDelegate];
}


//Handles touch events and updates color and crosshairs upon movement.
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];
    location.x -= _crossHair.centerCircleSize/2;
    location.y -= _crossHair.centerCircleSize/2;
    
    //update crosshair
    _crossHair.currentLocation = location;
    [_crossHair setNeedsDisplay];
    
    //update delegate values
    [self calcCbCrandCallbackDelegate];
    
}

//calcs colorlocation and calls back delegate.
-(void)calcCbCrandCallbackDelegate
{
    //calc x and y coordinates to call back to delegate to update any labels
    CGPoint colorLocation = _crossHair.currentLocation;
    colorLocation.x +=_crossHair.centerCircleSize/2;
    colorLocation.y +=_crossHair.centerCircleSize/2;
    
    int CB =floor(colorLocation.x/self.frame.size.width*256);
    double CR = floor(255 -(colorLocation.y/self.frame.size.height*255));
    
    //tell delegate that values have changed.
    [_delegate updateSelectedColor:[self convertYCbCrToRGBwithY:_yValue Cb:CB Cr:CR]];
    [_delegate updateCbVAlue:CB];
    [_delegate updateCrValue:CR];
}


//converts YCBCR to RGB (Formula used from http://www.equasys.de/colorconversion.html
-(UIColor *)convertYCbCrToRGBwithY:(float)y  Cb:(float)cb Cr:(float)cr
{
    float r,g,b = 0;
    r = y + (cr-128)*1.4;
    g = y + ((cb-128)*-0.343f) + ((cr - 128)*-0.711f);
    b = y + (cb - 128)*1.765;
    
    r = r/255;
    g = g/255;
    b = b/255;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}

//Adds visual constraints based upon input (this is a helper method to make above code less garish.)
- (void)addVisualConstraint:(NSString *)s  withBindings:(NSDictionary *)bindings
{
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:s options:0 metrics:nil views:bindings]];
    return;
}

@end
