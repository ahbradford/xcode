//
//  ABColorField.h
//  ColorPicker
//  Draws a YCBCR color field and handles touch events to set the included crosshair and update the Y value with an external
//  source.
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>


//AB Colorfield Delegeate declaration, probably should have done this differently.....
@protocol ABColorFieldDelegate <NSObject>

@required

-(void)updateSelectedColor:(UIColor *)color;
-(void)updateCrValue:(int)cR;
-(void)updateCbVAlue:(int)cB;


@end

@interface ABColorField : UIView

@property (nonatomic, weak) id <ABColorFieldDelegate> delegate;
@property double yValue;
//Recalculation of the color field can be expensive, set the rectSize property  large to draw
//larger squares when the Y value is in flux. Afterward, redraw with a smaller
//value to make a smooth gradient
@property int rectSize;
//Current selected color
@property (nonatomic) UIColor *color;
//Prevoiusly selected color
@property (nonatomic) UIColor *previousColor;

@end