//
//  ABColorPicker.h
//  ColorPicker
//  Creates an YCBCR ABColorPicker view which contains a slider and view with crosshairs, and a current selected color field.
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABColorField.h" 
#import "CCColorPickerProtocol.h"
@interface ABColorPicker : UIControl <ABColorFieldDelegate,CCColorPickerProtocol>

//sets and gets the currently selected color
@property (nonatomic) UIColor *color;
//sets and get the previous color
@property (nonatomic) UIColor *previousColor;

@end
