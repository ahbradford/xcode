//
//  ABColorCrossHair.m
//  ColorPicker
//
//  Created by adambradford on 2/1/13.
//  Copyright (c) 2013 Adam Bradford. All rights reserved.
//

#import "ABColorCrossHair.h"

@interface ABColorCrossHair()
@end

@implementation ABColorCrossHair

@synthesize currentLocation = _currentLocation;
@synthesize centerCircleSize = _centerCircleSize;
@synthesize lineWidth = _lineWidth;

//Initialize!!!
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _currentLocation = CGPointMake(30.0f, 30.0f);
        self.backgroundColor = [UIColor clearColor];
        _centerCircleSize = 20;
        _lineWidth = .5;
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}


//calculates current location and sets needs display
-(void)setCurrentLocation:(CGPoint)currentLocation
{
    
    if(currentLocation.x < -_centerCircleSize/2)currentLocation.x = -_centerCircleSize/2;
    if(currentLocation.x > self.bounds.size.width -_centerCircleSize/2-1)currentLocation.x = self.bounds.size.width -_centerCircleSize/2-1;
    if(currentLocation.y < -_centerCircleSize/2)currentLocation.y = -_centerCircleSize/2;
    if(currentLocation.y > self.bounds.size.height -_centerCircleSize/2-1)currentLocation.y = self.bounds.size.height -_centerCircleSize/2-1;
    _currentLocation = currentLocation;
    [self setNeedsDisplay];
}


//Draws circle with center at _currentLocation and crosshairs
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextAddEllipseInRect(context, CGRectMake(_currentLocation.x, _currentLocation.y, _centerCircleSize, _centerCircleSize));
    
    [[UIColor clearColor] setFill];
    [[UIColor blackColor]setStroke];
    
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGContextMoveToPoint(context, 0, _currentLocation.y+_centerCircleSize/2);
    CGContextAddLineToPoint(context, _currentLocation.x , _currentLocation.y+_centerCircleSize/2);
    
    CGContextMoveToPoint(context, _currentLocation.x+_centerCircleSize, _currentLocation.y+_centerCircleSize/2);
    CGContextAddLineToPoint(context, self.bounds.size.width , _currentLocation.y+_centerCircleSize/2);
    
    CGContextMoveToPoint(context, _currentLocation.x +_centerCircleSize/2, 0);
    CGContextAddLineToPoint(context, _currentLocation.x +_centerCircleSize/2 , _currentLocation.y);
    
    CGContextMoveToPoint(context, _currentLocation.x+_centerCircleSize/2, _currentLocation.y+_centerCircleSize);
    CGContextAddLineToPoint(context, _currentLocation.x+_centerCircleSize/2 , self.bounds.size.height);
    
    CGContextSetLineWidth(context, _lineWidth);
    
    CGContextDrawPath(context, kCGPathStroke);
}


@end

