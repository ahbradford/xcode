//
//  ResultViewController.m
//  BoggleClient
//
//  Created by Adam Bradford on 11/20/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import "ResultViewController.h"
#import "BoggleGameClient.h"

@interface ResultViewController () <BoggleGameClientDelegate>;
@end

@implementation ResultViewController
@synthesize resultArray = _resultArray;
@synthesize boggleGame = _boggleGame;
@synthesize p1Name = _p1Name;
@synthesize p2Name = _p2Name;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *winnerString;
    NSArray *scoreArray = self.resultArray[0];
    int p1FinalScore = [scoreArray[0] intValue];
    int p2FinalScore = [scoreArray[1] intValue];
    if(p1FinalScore > p2FinalScore)
    {
        winnerString = @"You Win!!";
    }
    else if(p1FinalScore < p2FinalScore)
    {
        winnerString = @"You Lose";
    }
    else
    {
        winnerString = @"Tied!";
    }
    
    self.title = winnerString;

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController popToRootViewControllerAnimated:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.resultArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
            return [self.resultArray[section] count];
          
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ResultCell";
    UITableViewCell *cell;
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    }
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
    }
    cell.detailTextLabel.text = @"";
    NSArray *currentArray = self.resultArray[indexPath.section];
    if(indexPath.section == 0)
    {
        if(indexPath.row == 0)
        {
            
            cell.textLabel.text = self.p1Name;
            cell.detailTextLabel.text = currentArray[indexPath.row];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            return cell;
            
        }
        else
        {
            cell.textLabel.text = self.p2Name;
            cell.detailTextLabel.text = currentArray[indexPath.row];
            cell.detailTextLabel.textColor = [UIColor blackColor];
            return cell;
        }
    }
    
    
    cell.textLabel.text = currentArray[indexPath.row];
    
    
    return cell;
}
                           
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return @"Scores";
            break;
        case 1:
            return @"Player Unique Legal Words Played";
            break;
        case 2:
            return @"Opponent Unique Legal Words Played";
            break;
        case 3:
            return @"Common Words Played";
            break;
        case 4:
            return @"Player Illegal Words Played";
            break;
        case 5:
            return @"Oppenent Illegal Words Played";
            break;
            
        default:
            return nil;
            break;
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}


@end
