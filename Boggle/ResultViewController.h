//
//  ResultViewController.h
//  BoggleClient
//
//  Created by Adam Bradford on 11/20/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BoggleGameClient.h"
@interface ResultViewController : UITableViewController
@property NSArray *resultArray;
@property BoggleGameClient* boggleGameClient;
@property NSString *p1Name;
@property NSString *p2Name;







@end
