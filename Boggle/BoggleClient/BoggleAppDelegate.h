//
//  BoggleAppDelegate.h
//  BoggleClient
//
//  Created by Adam Bradford on 11/13/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoggleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
