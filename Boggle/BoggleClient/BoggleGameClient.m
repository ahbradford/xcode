//
//  BoggleGameClient.m
//  BoggleClient
//
//  Created by Adam Bradford on 11/14/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import "BoggleGameClient.h"
#import "GCDAsyncSocket.h"



@interface BoggleGameClient()
@property NSLock *sendLock;
@property NSLock *receiveLock;
@property GCDAsyncSocket* socket;
@property NSArray *letterButtons;
@property NSNumber *bytesRead;
@property NSMutableArray* queue;
@property NSString *receiveString;
@property BOOL gameInProgress;
@property NSData *dataForNewLineString;
@property int timeOut;
@property NSString *host;
@property NSString *playerName;




@end

@implementation BoggleGameClient
@synthesize socket;
@synthesize boggleDelegate;
@synthesize gameInProgress;
@synthesize dataForNewLineString;
@synthesize timeOut;
@synthesize sendLock;
@synthesize receiveLock;
@synthesize host;
@synthesize playerName;


@synthesize receiveString;
-(BoggleGameClient *)initBoggleGameClientWithIPAddress:(NSString *)ipAdddress:(NSString *) pName
{
    self = [super init];
    if(self)
    {
        playerName = pName;
        host = ipAdddress;
        self.queue = [[NSMutableArray alloc]init];
        self.bytesRead = 0;
        self.receiveString = @"";
        dataForNewLineString = [@"\n" dataUsingEncoding:NSUTF8StringEncoding];
        
        self.socket  = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
        [socket connectToHost:host onPort:2000 error:nil];
        timeOut = -1;
        receiveString = @"";
        sendLock = [[NSLock alloc]init];
        receiveLock = [[NSLock alloc]init];
        return self;

    
    }
    return nil;
    
}

-(void)beginGame:(NSString *)name
{
    if(socket.isConnected)NSLog(@"connected");
    NSString *sendstring = @"PLAY ";
    sendstring = [sendstring stringByAppendingString:name];
    sendstring = [sendstring stringByAppendingString:@"\n"];
    NSData *sendData = [sendstring  dataUsingEncoding:NSUTF8StringEncoding ];
    [socket writeData:sendData withTimeout:timeOut tag:0];
    gameInProgress = true;
    [socket readDataToData:dataForNewLineString withTimeout:timeOut tag:0];
    return;
}

-(void)send:(NSString *)s
{
    
    NSData *sendData = [s  dataUsingEncoding:NSUTF8StringEncoding ];
    [socket writeData:sendData withTimeout:2000 tag:1];
    
    
}

-(void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)hostName port:(UInt16)port
{
    if([self.host isEqualToString:hostName])
    {
        [boggleDelegate socketHasConnected];
        [self beginGame:playerName];
    }
}

-(void)processReceive:(NSString *)s
{
    NSRange range = [s rangeOfString:@" "];
    if([s isEqualToString:@"TERMINATED"])
    {
        if(socket.isConnected == YES)
        {
            [socket disconnect];
            
        }
        [self.boggleDelegate gameHasTerminated];
        return;
    }
    
    NSString *command = [s substringToIndex:range.location];
    if([command isEqualToString: @"TIME"])
    {
        [boggleDelegate updateTime:[s substringFromIndex:range.location]];
    }
    if([command isEqualToString: @"START"])
    {
        NSArray *array = [s componentsSeparatedByString:@" "];
        NSString *boardString = array[1];
        NSString *time = array[2];
        
            
        NSMutableArray *boardArray = [[NSMutableArray alloc]initWithCapacity:[boardString length]];
        for (int i=0; i < [boardString length]; i++)
        {
            NSString *ichar = [NSString stringWithFormat:@"%c", [boardString characterAtIndex:i ]];
            
            [boardArray addObject:ichar];
        }
        NSString *opponent = array[3];
        [boggleDelegate beginGame:boardArray :playerName:opponent:time];
        return;
        
    }
    if([command isEqualToString: @"SCORE"])
    {
        NSArray *array = [s componentsSeparatedByString:@" "];
        NSString *p1Score = array[1];
        NSString *p2Score = array[2];
        [boggleDelegate updateScore:p1Score:p2Score];
        
        return;
        
    }
    
    if([command isEqualToString: @"STOP"])
    {
        

        NSMutableArray *array = [[s componentsSeparatedByString:@" "]mutableCopy];
        [array removeObjectAtIndex:0];
        
        //the order here is VERY important.
        NSArray *p1UniqueLegalWords = [self putWordsinArray:array];
        NSArray *p2UniqueLegalWords = [self putWordsinArray:array];
        NSArray *commonLegalWords = [self putWordsinArray:array];
        NSArray *p1IllegalWords = [self putWordsinArray:array];
        NSArray *p2IllegalWords = [self putWordsinArray:array];
        
        
        
        [boggleDelegate gameHasEnded:p1UniqueLegalWords :p2UniqueLegalWords :commonLegalWords:p1IllegalWords:p2IllegalWords];
       
        return;
        
    }
    
}
-(NSArray *)putWordsinArray:(NSMutableArray *)array
{
    NSMutableArray *wordList = [[NSMutableArray alloc]init];
    int wordNumber = [array[0]integerValue];
    [array removeObjectAtIndex:0];
    for(int i = 0; i < wordNumber; i ++)
    {
        [wordList addObject: array[0]];
        [array removeObjectAtIndex:0];
    }
return wordList;
}

- (void)socket:(GCDAsyncSocket *)sender didReadData:(NSData *)data withTag:(long)tag
{
    [receiveLock lock];
    NSString *tempString = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    tempString = [tempString substringToIndex:tempString.length -1];
        [self processReceive:tempString];
    [receiveLock unlock];
    [socket readDataToData:dataForNewLineString withTimeout:timeOut tag:0];
}

            // continued




@end

