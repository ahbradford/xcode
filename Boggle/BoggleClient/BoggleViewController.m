//
//  BoggleViewController.m
//  BoggleClient
//
//  Created by Adam Bradford on 11/13/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import "BoggleViewController.h"
#import "BoggleGameClient.h"

@interface BoggleViewController () <BoggleGameClientDelegate,UIPickerViewDelegate,UIPickerViewDataSource>;

@property int time;
@property NSArray *boardArray;
@property NSMutableArray *recentIPAddresses;
@property NSString *opponent;

@end

@implementation BoggleViewController
@synthesize ipAddressTextBox;
@synthesize playerNameTextBox;

@synthesize boggleGame;
@synthesize time;
@synthesize boardArray;
@synthesize opponent;
@synthesize activityIndicator;
@synthesize picker = _picker;
@synthesize recentIPAddresses = _recentIPAddresses;
@synthesize connectButton = _connectButton;

-(void)viewWillAppear:(BOOL)animated
{
   if(self != self.navigationController.viewControllers[0])
   {
       [self.navigationController popToRootViewControllerAnimated:NO];
   }
}

- (void)viewDidLoad

{
    
    [super viewDidLoad];

    self.picker.hidden = YES;
    NSArray *recentTempArray = [[NSUserDefaults standardUserDefaults]objectForKey:@"recentIPAddresses"];
    self.recentIPAddresses = [recentTempArray mutableCopy];
   
    
    if(!self.recentIPAddresses)
    {
        self.recentIPAddresses = [[NSMutableArray alloc]init];
        [[NSUserDefaults standardUserDefaults]setObject:self.recentIPAddresses forKey:@"recentIpAddresses"];
        
    }
    else
    {
        self.ipAddressTextBox.text = self.recentIPAddresses[0];
    }
    
    NSString *playerName = [[NSUserDefaults standardUserDefaults]objectForKey:@"playerName"];
    if(playerName)
    {
        self.playerNameTextBox.text = playerName;
    }
    else
    {
        self.playerNameTextBox.text = @"";
    }
    
    
    	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setIpAddressTextBox:nil];
    [self setPlayerNameTextBox:nil];

    [self setActivityIndicator:nil];
    [self setConnectButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)connect:(id)sender
{

    if ([self.connectButton.currentTitle isEqualToString:@"Cancel"])
    {
        boggleGame = nil;
        [activityIndicator stopAnimating];
        [self.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
        return;
    }
    
    
    if([self.playerNameTextBox.text isEqualToString: @""])
    {
        UIAlertView *disconnectedAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter Name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [disconnectedAlert show];
        return;
    }
    for(int i = 0; i < self.recentIPAddresses.count; i++)
    {
        NSString *tempAddress = self.recentIPAddresses[i];
        if([self.ipAddressTextBox.text isEqualToString:tempAddress])
        {
            [self.recentIPAddresses removeObjectAtIndex:i];
        }
    }
    
    [self.recentIPAddresses insertObject:self.ipAddressTextBox.text atIndex:0];
    [self.picker reloadAllComponents];
        
    
    while (self.recentIPAddresses.count > 10)
    {
        [self.recentIPAddresses removeLastObject];
    }
    [[NSUserDefaults standardUserDefaults]setObject:self.playerNameTextBox.text forKey:@"playerName"];
    [[NSUserDefaults standardUserDefaults]setObject:self.recentIPAddresses forKey:@"recentIPAddresses"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    self.picker.hidden = YES;
    
    
    boggleGame = [[BoggleGameClient alloc]initBoggleGameClientWithIPAddress:ipAddressTextBox.text:playerNameTextBox.text];
    [boggleGame setBoggleDelegate:self ];
    [self.connectButton setTitle:@"Cancel" forState:UIControlStateNormal];
    [activityIndicator startAnimating];
    
    return;
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"PlaySegue"])
    {
        UIViewController <BoggleGameClientDelegate> *newController = segue.destinationViewController;
        [boggleGame setBoggleDelegate:newController];
        newController.boggleGame = self.boggleGame;
        [activityIndicator stopAnimating];
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.connectButton setTitle:@"Connect" forState:UIControlStateNormal];
    }
}

-(void)socketHasConnected
{
   

    [self performSegueWithIdentifier:@"PlaySegue" sender:self];
}


- (IBAction)recent:(UIButton* )sender
{
    
    if(self.recentIPAddresses.count == 0)
    {
        [self.recentIPAddresses addObject:self.ipAddressTextBox.text];
    }
    if(self.picker.hidden == YES)
    {
        
        self.picker.hidden = NO;
        [sender setTitle:@"Hide Recents" forState:UIControlStateNormal];
    
    }
    else
    {
        self.picker.hidden = YES;
        [sender setTitle:@"Show Recents" forState:UIControlStateNormal];
    }

}

- (IBAction)backGroundTouched:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.picker.hidden = YES;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return self.recentIPAddresses.count;
}
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return self.recentIPAddresses[row];
}
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    int i = row;
    self.ipAddressTextBox.text = self.recentIPAddresses[i];
    
}

-(IBAction)unwindAll:(UIStoryboardSegue *)segue
{
    
}
@end
