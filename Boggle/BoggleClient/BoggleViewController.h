//
//  BoggleViewController.h
//  BoggleClient
//
//  Created by Adam Bradford on 11/13/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BoggleGameClient;

@interface BoggleViewController : UIViewController
- (IBAction)connect:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *ipAddressTextBox;
@property (weak, nonatomic) IBOutlet UITextField *playerNameTextBox;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *connectButton;
@property BoggleGameClient *boggleGame;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
- (IBAction)recent:(UIButton *)sender;
- (IBAction)backGroundTouched:(UIButton *)sender;




@end
