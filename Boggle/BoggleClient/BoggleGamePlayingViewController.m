//
//  BoggleGamePlayingViewController.m
//  BoggleClient
//
//  Created by adambradford on 11/17/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import "BoggleGamePlayingViewController.h"
#import "BoggleGameClient.h"
#import <AudioToolbox/AudioToolbox.h>


@interface BoggleGamePlayingViewController () <BoggleGameClientDelegate>

@property NSArray *boardArray;
@property NSArray *buttonArray;
@property BOOL gameInProgress;
@property NSArray *gameResults;



@end


@implementation BoggleGamePlayingViewController
@synthesize boardArray = _boardArray,buttonArray = _buttonArray ,boggleGame = _boggleGame;
@synthesize time = _time;
@synthesize activityIndicator = _activityIndicator;
@synthesize letter0 = _letter0,letter1 = _letter1,letter2 = _letter2,letter3 = _letter3,letter4 = _letter4,letter5 = _letter5,letter6 = _letter6,letter7 = _letter7,letter8 = _letter8,letter9 = _letter9,letter10 = _letter10,letter11 = _letter11,letter12 = _letter12,letter13 = _letter13,letter14 = _letter14,letter15 = _letter15;
@synthesize p1Name = _p1Name,p2Name = _p2Name,p1Score = _p1Score,p2Score = _p2Score,enter = _enter,backSpace = _backSpace,wordTextField = _wordTextField;
@synthesize gameInProgress = _gameInProgress;
@synthesize gameResults = _gameResults;

 
    
-(void)updateTime:(NSString *)newTime
{
    self.time.text = newTime;
}
-(void)updateScore:(NSString *)p1:(NSString *)p2
{
    if(self.p1Score.text.integerValue < p1.integerValue)
    {
        CFURLRef soundFileURLRef = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef)@"sonicRing", CFSTR ("mp3"), NULL);
        UInt32 sonicRingSound;
        AudioServicesCreateSystemSoundID(soundFileURLRef, &sonicRingSound);
        AudioServicesPlaySystemSound(sonicRingSound);
    }
    else if(self.p1Score.text.integerValue > p1.integerValue)
    {
        CFURLRef soundFileURLRef = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef)@"sonicKilled", CFSTR ("mp3"), NULL);
        UInt32 sonicKilledSound;
        AudioServicesCreateSystemSoundID(soundFileURLRef, &sonicKilledSound);
        AudioServicesPlaySystemSound(sonicKilledSound);
    }
    self.p1Score.text = p1;
    self.p2Score.text = p2;
}
    
-(void)beginGame: (NSArray *)board :(NSString *)p1:(NSString *)p2:(NSString *)gameTime;
    {
        
        
        self.gameInProgress = YES;
        [self.sendButton setTitle:@"" forState:UIControlStateNormal];
        [self putButtonsInArray];
        for(int i = 0; i < [board count]; i++)
        {
            UIButton *b = self.buttonArray[i];
            NSString *s = board[i];
            if([s isEqualToString:@"Q"])
            {
                s = @"Qu";
            }
            [b setTitle:s forState:UIControlStateNormal];
            [b setEnabled:YES];
           
            
        }
        self.time.text = gameTime;
        self.p1Name.text = p1;
        self.p2Name.text = p2;
        self.p1Score.text = @"0";
        self.p2Score.text = @"0";
       
        [self.activityIndicator stopAnimating];
        self.time.hidden = NO;
        self.navigationItem.title = @"PLAY!";
        
    }

-(void)gameHasTerminated
{
    UIAlertView *disconnectedAlert = [[UIAlertView alloc]initWithTitle:@"Server Connection To remote player:" message:@"The other player has disconnected, You WIN!!" delegate:self cancelButtonTitle:@"Return to Connect Screen" otherButtonTitles:nil, nil];
    [disconnectedAlert show];
   
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
       [[self navigationController]popToRootViewControllerAnimated:YES];
    }
}



-(void)gameHasEnded:(NSArray *)p1LegalUniqueWordsPlayed :(NSArray *)p2LegalUniqueWordsPlayed : (NSArray *)commonWordsPlayed:(NSArray *)p1IllegalWordsPlayed:(NSArray *)p2IllegalWordsPlayed
{
    NSArray *scores = [[NSArray alloc]initWithObjects:self.p1Score.text,self.p2Score.text, nil];
    self.gameInProgress = NO;
    self.time.text = 0;
    if(self.p1Score.text.integerValue >= self.p2Score.text.integerValue)
    {
        CFURLRef soundFileURLRef = CFBundleCopyResourceURL(CFBundleGetMainBundle(), (CFStringRef)@"sonicExtraLife", CFSTR ("mp3"), NULL);
        UInt32 sonicExtraLifeSound;
        AudioServicesCreateSystemSoundID(soundFileURLRef, &sonicExtraLifeSound);
        AudioServicesPlaySystemSound(sonicExtraLifeSound);
    }
    else
    {
        NSString *sonicEndSoundPath = [[NSBundle mainBundle]pathForResource:@"sonicEnd" ofType:@"mp3"];
        SystemSoundID sonicEndID;
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath:sonicEndSoundPath], &sonicEndID);
        AudioServicesPlaySystemSound(sonicEndID);
    }
    
    NSMutableArray *resultArray = [[NSMutableArray alloc]initWithCapacity:5];
    [resultArray addObject:scores];
    [resultArray addObject:p1LegalUniqueWordsPlayed];
    [resultArray addObject:p2LegalUniqueWordsPlayed ];
    [resultArray addObject:commonWordsPlayed];
    [resultArray addObject:p1IllegalWordsPlayed];
    [resultArray addObject:p2IllegalWordsPlayed];
    self.gameResults = resultArray;
    [self performSegueWithIdentifier:@"ResultSegue" sender:self];
    
    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"ResultSegue"])
    {
        UIViewController <BoggleGameClientDelegate> *newController = segue.destinationViewController;
        newController.resultArray = self.gameResults;
        newController.p1Name = self.p1Name.text;
        newController.p2Name = self.p2Name.text;
    }
}
- (IBAction)letterPressed:(UIButton *)sender
{
    if(self.gameInProgress)
    {
        NSString *letterPressed = sender.currentTitle;
        if([letterPressed isEqualToString:@"Qu"])
        {
            letterPressed = @"QU";
        }
        
        [self.sendButton setTitle:[self.sendButton.currentTitle stringByAppendingString:letterPressed]forState:UIControlStateNormal];
    }
    
}

- (IBAction)sendWord:(UIButton *)sender
{
    if(self.gameInProgress)
    {
        NSString *stringToSend = @"WORD ";
        stringToSend = [stringToSend stringByAppendingString:[self.sendButton titleForState:UIControlStateNormal]];
        stringToSend = [NSString stringWithFormat:@"%@\n",stringToSend];
        if([stringToSend isEqualToString:@"WORD \n"])
        {
            [self.sendButton setTitle:@"" forState:UIControlStateNormal];
            return;
        }
        [self.boggleGame send:stringToSend];
        [self.sendButton setTitle:@"" forState:UIControlStateNormal];
    }
}

- (IBAction)backSpace:(UIButton *)sender
{
    if(self.gameInProgress)
    {
        
        NSString *tempString = [self.sendButton titleForState:UIControlStateNormal];
        if(tempString.length >0)
        {
            [self.sendButton setTitle:[tempString substringToIndex:tempString.length-1] forState:UIControlStateNormal];
        }
       
    }
}
-(void)putButtonsInArray
{
    NSMutableArray *array = [[NSMutableArray alloc]initWithCapacity:16];
    array[0]=self.letter0;
    array[1]=self.letter1;
    array[2]=self.letter2;
    array[3]=self.letter3;
    array[4]=self.letter4;
    array[5]=self.letter5;
    array[6]=self.letter6;
    array[7]=self.letter7;
    array[8]=self.letter8;
    array[9]=self.letter9;
    array[10]=self.letter10;
    array[11]=self.letter11;
    array[12]=self.letter12;
    array[13]=self.letter13;
    array[14]=self.letter14;
    array[15]=self.letter15;
    self.buttonArray = array;
    for(int i = 0; i < self.buttonArray.count; i ++)
    {
        UIButton * tempButton = self.buttonArray[i];
        [tempButton setEnabled:NO];
    }
    return;
    
}


- (void)viewDidUnload {
    [self setLetter0:nil];
    [self setLetter1:nil];
    [self setLetter2:nil];
    [self setLetter3:nil];
    [self setLetter4:nil];
    [self setLetter5:nil];
    [self setLetter6:nil];
    [self setLetter7:nil];
    [self setLetter8:nil];
    [self setLetter9:nil];
    [self setLetter10:nil];
    [self setLetter11:nil];
    [self setLetter12:nil];
    [self setLetter13:nil];
    [self setLetter14:nil];
    [self setLetter15:nil];
    [self setP1Score:nil];
    [self setP2Score:nil];
    [self setTime:nil];
    [self setP1Name:nil];
    [self setP2Name:nil];
    [self setWordTextField:nil];
    [self setEnter:nil];
    [self setBackSpace:nil];
    [self setActivityIndicator:nil];
    [self setBackSpace:nil];
    [super viewDidUnload];
}

@end
