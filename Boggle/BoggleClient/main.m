//
//  main.m
//  BoggleClient
//
//  Created by Adam Bradford on 11/13/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BoggleAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BoggleAppDelegate class]));
    }
}
