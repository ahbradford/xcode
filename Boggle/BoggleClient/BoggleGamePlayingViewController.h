//
//  BoggleGamePlayingViewController.h
//  BoggleClient
//
//  Created by adambradford on 11/17/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BoggleGameClient;

@interface BoggleGamePlayingViewController : UIViewController
@property BoggleGameClient *boggleGame;
@property (weak, nonatomic) IBOutlet UIButton *letter0;
@property (weak, nonatomic) IBOutlet UIButton *letter1;
@property (weak, nonatomic) IBOutlet UIButton *letter2;
@property (weak, nonatomic) IBOutlet UIButton *letter3;
@property (weak, nonatomic) IBOutlet UIButton *letter4;
@property (weak, nonatomic) IBOutlet UIButton *letter5;
@property (weak, nonatomic) IBOutlet UIButton *letter6;
@property (weak, nonatomic) IBOutlet UIButton *letter7;
@property (weak, nonatomic) IBOutlet UIButton *letter8;
@property (weak, nonatomic) IBOutlet UIButton *letter9;
@property (weak, nonatomic) IBOutlet UIButton *letter10;
@property (weak, nonatomic) IBOutlet UIButton *letter11;
@property (weak, nonatomic) IBOutlet UIButton *letter12;
@property (weak, nonatomic) IBOutlet UIButton *letter13;
@property (weak, nonatomic) IBOutlet UIButton *letter14;
@property (weak, nonatomic) IBOutlet UIButton *letter15;
@property (weak, nonatomic) IBOutlet UILabel *p1Score;
@property (weak, nonatomic) IBOutlet UILabel *p2Score;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UILabel *p1Name;
@property (weak, nonatomic) IBOutlet UILabel *p2Name;
@property (weak, nonatomic) IBOutlet UITextField *wordTextField;
@property (weak, nonatomic) IBOutlet UIButton *enter;
@property (weak, nonatomic) IBOutlet UIButton *backSpace;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
- (IBAction)letterPressed:(id)sender;
- (IBAction)sendWord:(UIButton *)sender;
- (IBAction)backSpace:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

@end
