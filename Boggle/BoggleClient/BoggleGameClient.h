//
//  BoggleGameClient.h
//  BoggleClient
//
//  Created by Adam Bradford on 11/14/12.
//  Copyright (c) 2012 Adam Bradford. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BoggleGameClient;

@protocol BoggleGameClientDelegate <NSObject>

@required
@property BoggleGameClient *boggleGame;

@optional
@property NSString *p1Name;
@property NSString *p2Name;
@property NSArray *resultArray;
-(void)updateTime:(NSString *)time;
-(void)updateScore:(NSString *)p1Score:(NSString *)p2Score;
-(void)beginGame: (NSArray *)board :(NSString *)p1:(NSString *)p2:(NSString *)gameTime;
-(void)socketHasConnected;
-(void)gameHasBegun;
-(void)gameHasTerminated;
-(void)gameHasEnded:(NSArray *)p1LegalUniqueWordsPlayed :(NSArray *)p2LegalUniqueWordsPlayed : (NSArray *)commonWordsPlayed:(NSArray *)p1IllegalWordsPlayed:(NSArray *)p2IllegalWordsPlayed;


@end;



@interface BoggleGameClient : NSObject <NSStreamDelegate>


@property (nonatomic, weak) id <BoggleGameClientDelegate> boggleDelegate;

-(BoggleGameClient *)initBoggleGameClientWithIPAddress:(NSString *)ipAddress:(NSString *)playerName;
-(void)beginGame:(NSString *)name;
-(void)send:(NSString *)sendString;

@end
